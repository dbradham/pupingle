import React from 'react';
import { View, StyleSheet } from 'react-native';
import CustomTabButton from './CustomTabButton';

const CustomTabBar = ({ navigation }) => {
    return <View style={styles.container}>
        <CustomTabButton navigation={navigation} route="Timeline" />
        <CustomTabButton navigation={navigation} route="Calendar" />
        <CustomTabButton navigation={navigation} route="Profile" />
        <CustomTabButton navigation={navigation} route="Posts" />
        <CustomTabButton navigation={navigation} route="Neighborhood" />
    </View>
};

export default CustomTabBar;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        height: 50,
        backgroundColor: 'white',
        justifyContent: 'space-around',
        borderTopColor: 'black',
        borderTopWidth: 2,
    }
});