import React, { useContext, useEffect, useState } from "react";
import {
    Image,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    Platform,
    Text
} from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import AccountProfileUserDetail from './AccountProfileUserDetail';
import DogScroll from "./DogScroll";
import { Context } from '../context/AuthContext';
import firebase from '../../firebase';
import { Ionicons } from "@expo/vector-icons";
import { navigate } from '../navigationRef';

Font.loadAsync({
    'sf-rounded-pro-bold': require('../assets/fonts/SF-Pro-Rounded-Bold.otf'),
    'sf-rounded-pro-regular': require('../assets/fonts/SF-Pro-Rounded-Regular.otf'),
    'sf-rounded-pro-thin': require('../assets/fonts/SF-Pro-Rounded-Thin.otf')
});

const getDogsForUserFromFirestore = async (uid) => {
    const dogs = await firebase
    .firestore()
    .collection('dogs')
    .where('uid', '==', uid)
    .get()
    .then((query) => {
      return query.docs.map((doc) => {
          return doc.data();
      });
    });
    return dogs;
  };

const AccountProfile = (props) => {
    const data = [
        {
            key: '0',
        },
        {
            key: '1',
        },
        {
            key: '2',
        },
        {
            key: '3',
        },
        {
            key: '4',
        },
        {
            key: '5',
        },
        {
            key: '6',
        },
        {
            key: '7',
        },
        {
            key: '8',
        },
        {
            key: '9',
        },
        {
            key: '10',
        },
        {
            key: '11',
        },
        {
            key: '12',
        },
        {
            key: '13',
        },
        {
            key: '14',
        },
    ];

    const [dogs, setDogs] = useState([]);
    const [user, setUser] = useState('');
    const [currentUser, setCurrentUser] = useState('');

    const navParams = props.navigation.state.params;
    const { state, addBuddy } = useContext(Context);

    const [isCurrentUser, setIsCurrentUser] = useState(true);

    useEffect(() => {
        if (navParams != null) {
            setUser(navParams.user);
            if (navParams.user.uid == state.user.uid) {
                setDogs(state.dogs);
            } else {
                setIsCurrentUser(false);
                setCurrentUser(state.user);
                getDogsForUserFromFirestore(navParams.user.uid).then((userDogs) => {
                    setDogs(userDogs);
                });
            }
        } else {
            setUser(state.user);
            setDogs(state.dogs);
        }
    }, []);

    const src = require('../assets/images/dog1.png');
    const src3 = require('../assets/images/dog3.png');

    const addIcon = Platform.OS === 'ios'
    ? 'ios-add-circle': 'md-add-circle';

    const messageIcon = Platform.OS === 'ios'
    ? 'ios-mail': 'md-mail';

    const editIcon = Platform.OS === 'ios'
    ? 'ios-add-circle': 'md-add-circle';

    let button; 
    switch (isCurrentUser) {
        case false:
            const currentUserFirstName = currentUser.firstName;
            const currentUserId = currentUser.uid;
            const otherUserId = user.uid;
            if (currentUser.buddies.includes(user.uid)) {
                button = <TouchableOpacity style={styles.buddyArea} onPress={() => navigate("Convo", { chat: {
                    chatters: [otherUserId, currentUserId],
                    metaData: [
                        {
                            id: currentUserId,
                            firstName: currentUser.firstName,
                            lastName: currentUser.lastName,
                        },
                        {
                            id: otherUserId,
                            firstName: user.firstName,
                            lastName: user.lastName,
                        }
                    ]
                } })}>
                    <Ionicons size={35} name={messageIcon} color="black" />
                    <Text style={styles.buddyText}>Send Message</Text>
                </TouchableOpacity>;
            } else {
                button = <TouchableOpacity style={styles.buddyArea} onPress={() => addBuddy(user.uid, currentUser)}>
                    <Ionicons size={35} name={addIcon} color="black" />
                    <Text style={styles.buddyText}>Add Buddy!</Text>
                </TouchableOpacity>;
            }
            break;
        case true:
            button = <TouchableOpacity style={styles.buddyArea} onPress={() => console.log('editing profile')}>
                <Text style={styles.buddyText}>Edit Profile</Text>
            </TouchableOpacity>;
            
    }

    return (
        <LinearGradient
            colors={["#09C6F9", "#045DE9"]}
            style={styles.container}>
            <AccountProfileUserDetail 
                user={user} 
                isCurrentUser={isCurrentUser} 
                src={src}/>
            {button}
            <DogScroll user={user} dogs={dogs} navigation={props.navigation} />
            <FlatList
                contentContainerStyle={styles.listContainer}
                data={data}
                renderItem={({ item }) => (
                    <TouchableOpacity>
                        <Image source={src3} style={styles.image} />
                    </TouchableOpacity>
                    )}
                keyExtractor={item => item.key}
                numColumns={3} />
        </LinearGradient>
    );
};

export default AccountProfile;

const colors = require('../static.json');

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        paddingTop: 60
    },
    name: {
        fontSize: 35,
        fontFamily: 'sf-rounded-pro-bold',
        color: colors.darkGrey,
        marginTop: 10
    },
    neighborhood: {
        fontSize: 16,
    },
    stats: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%'
    },
    picture: {
        borderWidth: 2,
        borderColor: '#EF8912',
        height: 150,
    },
    listContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    image: {
        marginVertical: 1,
        marginHorizontal: 1,
        height: 100,
        width: deviceWidth * 0.325,
    },
    icon: {
        color: 'black',
    },
    iconContainer: {
        marginHorizontal: 10,
    },
    buddyArea: {
        alignSelf: 'flex-start',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: deviceWidth * 0.1,
        width: deviceWidth * 0.8,
        borderRadius: 10,
        marginTop: 5,
        backgroundColor: 'white'
    },
    buddyText: {
        fontWeight: 'bold',
        color: 'black',
        marginLeft: 5,
        fontSize: 20
    },
});