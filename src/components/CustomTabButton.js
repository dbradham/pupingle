import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const CustomTabButton = ({ navigation, route }) => {
    let source = '';
    switch(route) {
        case 'Timeline':
            source = require('../assets/images/home.png');
            break;
        case 'Calendar':
            source = require('../assets/images/calendar.png');
            break;
        case 'Profile':
            source = require('../assets/images/profile.png');
            break; 
        case 'Posts':
            source = require('../assets/images/posts.png');
            break; 
        case 'Neighborhood':
            source = require('../assets/images/neighborhoods.png');
            break;        
    }

    return (
        <TouchableOpacity style={styles.container} onPress={() => navigation.navigate(route)}>
                <Image source={source} style={styles.icon} />
        </TouchableOpacity>
    );
};

export default CustomTabButton;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        width: 44,
        height: 44,
    },
});