import React from "react";
import { FlatList, StyleSheet } from "react-native";
import CustomHeader from '../CustomHeader';
import NotificationTile from './NotificationTile';

const NotificationList = ({ navigation }) => {
    const data = [{id: '1'}, {id: '2'}, {id: '3'}, {id: '4'}];
    return (
        <>
            <CustomHeader title="Notifications" navigation={navigation} />
            <FlatList
                data={data}
                renderItem={({ item }) => (
                    <NotificationTile navigation={navigation} />
                )}
                keyExtractor={item => item.id}
                style={styles.list}
            />
        </>
    );
};

export default NotificationList;

const styles = StyleSheet.create({
    list: {

    }
});