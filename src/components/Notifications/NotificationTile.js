import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';

const NotificationTile = ({ navigation }) => {
    return (
        <TouchableOpacity style={styles.container}
            onPress={()=>navigation.navigate("SpecificNotification")}>
            <View style={styles.userArea}>
                <Image source={require('../../assets/images/dog4.png')} style={styles.img}/>
                <View style={styles.userAreaCol}>
                    <Text>Cpt_Manzi</Text>
                    <Text>W/ Conan</Text>
                </View>
            </View>
            <Text style={styles.centralText}>Buddied you.</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Convo', { chat: {
                    chatters: ['otherUserId', 'currentUserId'],
                    metaData: [
                        {
                            id: 'currentUserId',
                            firstName: 'currentUser.firstName',
                            lastName: 'currentUser.lastName',
                        },
                        {
                            id: 'otherUserId',
                            firstName: 'user.firstName',
                            lastName: 'user.lastName',
                        }
                    ]
                }})}>
                <Text style={styles.rightText}>Message</Text>
            </TouchableOpacity>
        </TouchableOpacity>
    );
};

export default NotificationTile;

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        height: 75,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    userArea: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    userAreaCol: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 5,
    },
    img: {
        height: 50,
        width: 50,
        borderRadius: 25
    },
    centralText: {
        fontStyle: 'italic'
    },
    rightText: {
        borderWidth: 2,
        borderColor: '#cccccc',
        padding: 5,
        borderRadius: 5
    }
});