import React, { Component } from "react";
import { Button, View } from "react-native";
import CustomHeader from '../CustomHeader';

export default class SpecificNotification extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "orange" }}>
                <CustomHeader navigation={this.props.navigation} />
                <Button
                    title="To Notifications"
                    onPress={() => this.props.navigation.navigate("NotificationList")}
                />
            </View>
        );
    }
}