import React from "react";
import { View, Button, StyleSheet, Picker, Dimensions } from "react-native";
import CustomHeader from '../CustomHeader';
import CalendarRow from './CalendarRow';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const Secondary = (props) => {
    return <View style={styles.container}>
        <CustomHeader
            title="Calendar"
            navigation={props.navigation}
        />
        <Button
            title="Back"
            onPress={() => props.navigation.navigate("Main")}
        />
        <View style={styles.pickers}>
            <Picker
                selectedValue="all"
                prompt="Gender"
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                    console.log(itemIndex, itemValue)
                }>
                <Picker.Item label="All Genders" value="all" />
                <Picker.Item label="Male" value="male" />
                <Picker.Item label="Female" value="female" />
            </Picker>
            <Picker
                selectedValue="all"
                prompt="Distance"
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                    console.log(itemIndex, itemValue)
                }>
                <Picker.Item label=">1 mile" value=">1" />
                <Picker.Item label=">5 miles" value=">5" />
                <Picker.Item label=">15 miles" value=">15" />
                <Picker.Item label="Any Distance" value="all" />
            </Picker>
            <Picker
                selectedValue="all"
                prompt="Breeds"
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                    console.log(itemIndex, itemValue)
                }>
                <Picker.Item 
                    label="Golden Retriever"
                    value="goldenRetriever"
                />
                <Picker.Item 
                    label="German Shepard"
                    value="germanShepard"
                />
                <Picker.Item
                    label="All Breeds"
                    value="all"
                />
            </Picker>
        </View>
        <View style={styles.rowContainer}>
            <CalendarRow />
            <CalendarRow />
        </View>
    </View >
}
export default Secondary;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    rowContainer: {
        display: 'flex',
        flexDirection: 'column',
    },
    pickers: {
        display: 'flex',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'black',
    },
    picker: {
        height: 50,
        width: deviceWidth * 0.3,
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
    }
});