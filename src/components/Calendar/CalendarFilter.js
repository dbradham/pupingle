import React from "react";
import { View,
StyleSheet,
Dimensions } from "react-native";
import ModalDropdown from 'react-native-modal-dropdown';

const CalendarRow = (props) => {
    return <View style={styles.container}>
            <ModalDropdown defaultValue="Gender" style={styles.dropDown}
                options={['All Genders', 'Male', 'Female']}
                dropdownStyle={styles.innerDropDown}
                dropdownTextStyle={styles.dropdownTextStyle}
                textStyle={styles.textStyle}/>
            
            <ModalDropdown defaultValue="Distance" style={styles.dropDown}
                options={['<1 Mile', '<5 Miles', '<15 Miles', 'Any Distance']}
                dropdownStyle={styles.innerDropDown}
                dropdownTextStyle={styles.dropdownTextStyle}
                textStyle={styles.textStyle}/>

            <ModalDropdown defaultValue="Breed" style={styles.dropDown}
                options={['All Breeds', 'Golden Retriever', 'German Shepard', 'Pitbull', 'Bulldog']}
                dropdownStyle={styles.innerDropDownBreed}
                dropdownTextStyle={styles.dropdownTextStyle}
                textStyle={styles.textStyle}/>
    </View>
}
export default CalendarRow;

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        borderBottomWidth: 1,
        justifyContent: 'space-around',
    },
    dropDown: {
        height: 40,
        width: deviceWidth * 0.275,
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    innerDropDown: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        width: deviceWidth * 0.265,
    },
    dropdownTextStyle: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    innerDropDownBreed: {
        alignSelf:'flex-end',
    },
    textStyle: {
        alignSelf: 'center',
    },
});