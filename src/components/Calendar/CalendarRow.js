import React from "react";
import { View,
Text,
StyleSheet,
Image,
TouchableOpacity } from "react-native";
import CustomIcon from '../CustomIcon';

const CalendarRow = (props) => {
    const src3 = require('../../assets/images/dog3.png');
    const pin = require('../../assets/images/location.png');
    return <View style={styles.container}>
        <View style={styles.row}>
            <View style={styles.dogInfo}>
                <Image source={src3} style={styles.image} />
                <View style={styles.dogWords}>
                    <Text style={styles.accountText}>Cpt_Manzi</Text>
                    <Text style={styles.dogText}>W/ Conan</Text>
                </View>
            </View>
            <View style={styles.locationCol}>
                <CustomIcon source={pin} />
                <Text>2.5mi</Text>
            </View>
            <TouchableOpacity onPress={() => props.navigation.navigate("Convo", {
                name: "Cpt_Manzi"
            })} style={styles.messageButton}>
                <Text>Message</Text>
            </TouchableOpacity>
        </View >
        <Text style={styles.timeText}>{props.description}</Text>
    </View>
}
export default CalendarRow;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#dddddd',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 25,
        marginRight: 10,
    },
    row: {
        marginTop: 5,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: 100,
        width: '100%',
        justifyContent: 'space-between',
    },
    dogInfo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    dogWords: {
        display: 'flex',
        flexDirection: 'column',
    },
    locationCol: {
        display: 'flex',
        flexDirection: 'column',
    },
    image: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginHorizontal: 10,
    },
    messageButton: {
        borderWidth: 1,
        borderColor: 'black',
        padding: 5,
        marginRight: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '40%',
        borderRadius: 10,
    },
    accountText: {
        fontSize: 16,
    },
    dogText: {
        fontSize: 14,
    },
    timeText: {
        fontSize: 14,
        color: '#EF8912',
        marginBottom: 10,
    }
});