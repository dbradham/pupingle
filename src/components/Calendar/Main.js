import React, { Component } from 'react';
import { Alert,
StyleSheet,
Text,
View,
TouchableOpacity,
Modal,
Platform,
Dimensions} from 'react-native';
import { Agenda } from 'react-native-calendars';
import firebase from '../../../firebase';
import CalendarRow from './CalendarRow';
import CustomHeader from '../CustomHeader';
import CalendarFilter from './CalendarFilter';
import { Ionicons } from "@expo/vector-icons";
import { Item, Input, Label } from "native-base";

export default class AgendaScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: {},
      dbItems: {},
      modalVisible: false,
      description: null,
      location: null,
      start: null,
      end: null,
    };
  }

  getDateString() {
    const d = new Date();
    let month = d.getMonth() + 1;
    if (month < 9) {
      month = '0' + month;
    }
    let day = d.getDate();
    if (day < 9) {
      day = '0' + day;
    }
    const year = d.getFullYear();

    return [year, month, day].join('-');
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  postEvent() {
    const location = this.state.location;
    const description = this.state.description;
    const uid = this.props.screenProps.user;
    console.log('uid', uid);

    const start = new Date();
    const end = start.setHours(start.getHours() + 1);

    const calendarItem = {
      uid: 2,
      dogId: 12,
      location: location,
      description: description,
      start: start,
      end: end,
    }

    const calendarItems = firebase.firestore().collection('calendarItems');
    async function add(ci) {
      await calendarItems.add(ci);
    }
    add(calendarItem);
  }

  render() {
    const dateString = this.getDateString();

    const addIcon = Platform.OS === 'ios'
    ? 'ios-add-circle': 'md-add-circle';

    const quitIcon = Platform.OS === 'ios'
    ? 'ios-close-circle' : 'md-close-circle';

    const checkIcon = Platform.OS === 'ios'
      ? 'ios-checkmark-circle' : 'md-checkmark-circle';

    return (
      <View style={{height: '100%'}}>
        <Modal animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
          }}>
            <View style={styles.modal}>
              <Text style={{color: 'white'}}>Let people know what you're doing</Text>

              <Item floatingLabel
                    style={styles.item}>
                <Label>Description</Label>
                  <Input
                    style={styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText={description => this.setState({ description })} />
              </Item>
              <Item floatingLabel
                   style={styles.item}>
                <Label>Location</Label>
                  <Input
                    style={styles.input}
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText={location => this.setState({ location })} />
              </Item>
              <TouchableOpacity onPress={() => this.postEvent()}>
                        <Ionicons size={50} name={checkIcon} style={styles.checkIcon} />
              </TouchableOpacity>

              <TouchableOpacity>
                        <Ionicons onPress={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }} size={50} name={quitIcon} style={styles.quitIcon} />
              </TouchableOpacity>
            </View>
        </Modal>

        <CustomHeader title="Calendar" navigation={this.props.navigation} />
        <CalendarFilter />
        <TouchableOpacity onPress={() => this.setModalVisible(true)}>
          <Ionicons name={addIcon} style={styles.addIcon} size={50}/>
        </TouchableOpacity>
        <Agenda
          items={this.state.items}
          loadItemsForMonth={this.loadItems.bind(this)}
          selected={dateString}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          pastScrollRange={0}
          // markingType={'period'}
          // markedDates={{
          //    '2017-05-08': {textColor: '#43515c'},
          //    '2017-05-09': {textColor: '#43515c'},
          //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
          //    '2017-05-21': {startingDay: true, color: 'blue'},
          //    '2017-05-22': {endingDay: true, color: 'gray'},
          //    '2017-05-24': {startingDay: true, color: 'gray'},
          //    '2017-05-25': {color: 'gray'},
          //    '2017-05-26': {endingDay: true, color: 'gray'}}}
          // monthFormat={'yyyy'}
          // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
          // renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
          // hideExtraDays={false}
        />
      </View>
    );
  }

  loadItems(day) {
    let newItems = {};

    firebase
    .firestore()
    .collection('calendarItems')
    .onSnapshot((snapshot) => {
        const dbItems = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data()
        }));
        this.state.dbItems = dbItems;

        dbItems.forEach((item) => {
            let itemSeconds = item.start.seconds;
            var offset = new Date().getTimezoneOffset();
            itemSeconds = itemSeconds - (offset * 60)
            let itemDate = new Date(itemSeconds * 1000).toISOString().slice(0,10);
            if(newItems.hasOwnProperty(itemDate)){
                newItems[itemDate].push({
                    "height": 75,
                    "name": item.description
                });
            } else {
                newItems[itemDate] = [{
                    "height": 75,
                    "name": item.description
                }];
            }
        })

        for (let i = -15; i < 85; i++) {
            const time = day.timestamp + i * 24 * 60 * 60 * 1000;
            const strTime = this.timeToString(time);
            if (!newItems[strTime]) {
                newItems[strTime] = [];
            }
          }
          Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
          this.setState({
            items: newItems
          });
    });
  }

  renderItem(item) {
    return <CalendarRow navigation={this.props.navigation} description={item.name} />;

    return (
      <TouchableOpacity 
        style={[styles.item, {height: item.height}]} 
        onPress={() => Alert.alert(item.name)}
      >
        <Text>{item.name}</Text>
      </TouchableOpacity>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  addIcon: {
    //position: 'relative',
    //top: deviceHeight * 0.25,
    //left: deviceWidth * 0.15,
    color: 'blue',
  },
  modal: {
    height: deviceHeight * 0.45,
    width: deviceWidth * 0.8,
    borderRadius: deviceWidth * 0.2,
    backgroundColor: 'black',
    marginTop: deviceHeight * 0.1,
    marginHorizontal: deviceWidth * 0.1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  quitIcon: {
    color: 'red',
  },
  checkIcon: {
    color: 'white',
  },
  item: {
    height: 50,
    width: '75%',
    backgroundColor: '#444444',
    marginVertical: 10,
  }
});