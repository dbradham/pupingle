import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CustomIcon from './CustomIcon';

const CustomDrawerItem = ({ scene }) => {
    const route = scene.route.routeName;
    if (route === 'TabStack' || route === 'SearchTabNavigator') {
        return null;
    }

    let icon = null;
    let label = route.substring(0, route.length - 12);
    let containerStyle = styles.container;
    if (label === 'Settings') {
        const settingsIcon = require('../assets/images/settings.png');
        icon =  <CustomIcon source={settingsIcon} style={styles.icon} />;
        label = "Settings & Privacy";
    } else if (label === 'Messages') {
        const messageIcon = require('../assets/images/messages.png');
        icon = <CustomIcon source={messageIcon} style={styles.icon} />;
    } else if (label === 'Neighborhood') {
        return null;
        const neighborhoodIcon = require('../assets/images/neighborhoods.png');
        icon = <CustomIcon source={neighborhoodIcon} style={styles.icon} />;
    } else if (label === 'Groups') {
        const groupsIcon = require('../assets/images/groups.png');
        icon = <CustomIcon source={groupsIcon} style={styles.icon} />;
    } else if (label == 'Notificactions') {
        const postsIcon = require('../assets/images/alarm.png');
        icon = <CustomIcon source={postsIcon} style={styles.icon} />;
    } else if (label == 'Help') {
        const helpIcon = require('../assets/images/help.png');
        icon = <CustomIcon source={helpIcon} style={styles.icon} />;
        label = "Help & Support";
        containerStyle = styles.helpContainer;
    }

    return (
        <View style={containerStyle}>
            <View style={styles.iconContainer}>
                {icon}
            </View>
            <View styles={styles.labelContainer}>
                <Text style={styles.label}>
                    {label}
                </Text>
            </View>
        </View>
    );
};

export default CustomDrawerItem;

const styles = StyleSheet.create({
    container: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 10,
        marginLeft: 15,
    },
    helpContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 10,
        marginTop: 35,
        marginLeft: 15,
    },
    labelContainer: { 
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
    },
    iconContainer: { 
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: { 
        height: 35,
        width: 35,
    },
    label: {
        color: '#707070',
        marginLeft: 5,
        fontWeight: 'bold',
    }
});