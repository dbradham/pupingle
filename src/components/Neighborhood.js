import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'

const Neighborhood = (props) => {
    return <View>
        <TouchableOpacity>
            <Text
                style={styles.item}
            >{props.neighborhood}
            </Text>
        </TouchableOpacity>
    </View>
}

export default Neighborhood;
const styles = StyleSheet.create({
    item: {
        fontSize: 18,
        color: 'black',
        fontStyle: 'italic',
        // shadowColor: '#000000',
        // shadowOffset: {
        //     width: 0,
        //     height: 2
        // },
        // shadowRadius: 2,
        // shadowOpacity: 1.0,
    }
});
export default Neighborhood;