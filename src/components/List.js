import React, { useState, useContext } from 'react';
import {
    StyleSheet,
    FlatList,
} from 'react-native';
import Post from './Post';
import firebase from '../../firebase';
import { Context } from '../context/AuthContext';

const List = ({ navigation, mode }) => {
    const [posts, setPosts] = useState([]);

    const postSet = (mode, user) => {
        if (mode == 'browse') {
            firebase
            .firestore()
            .collection('posts')
            .onSnapshot((snapshot) => {
                const allPosts = snapshot.docs.map((doc) => ({
                    id: doc.id,
                    ...doc.data()
                })).sort(custom_sort);
    
                setPosts(allPosts);
            });
        } else {
            firebase
            .firestore()
            .collection('posts')
            .where('user', '==', user)
            .onSnapshot((snapshot) => {
                const userPosts = snapshot.docs.map((doc) => ({
                    id: doc.id,
                    ...doc.data()
                })).sort(custom_sort);

                setPosts(userPosts);
            });
        }
    };

    const { state } = useContext(Context);
    const user = state.user;
    postSet(mode, user);

    return (
            <FlatList
                data={posts}
                renderItem={({ item }) => (
                    <Post
                        post={item}
                        navigation={navigation} 
                    />
                )}
                keyExtractor={item => item.body}
            />
    );
};

export default List;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
});

function custom_sort(a, b) {
    return a.dateTime.seconds < b.dateTime.seconds;
};