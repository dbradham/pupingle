import React, { useState } from 'react';
import { 
    View, 
    TextInput, 
    Text, 
    StyleSheet, 
    TouchableOpacity 
} from 'react-native';
import firebase from '../../../firebase';

const sendMessage = async (newMessage, toId, fromId, chatId, toName, fromName) => {
    firebase.firestore()
    .collection('messages')
    .add(newMessage);

    const chats = firebase.firestore().collection('chats');

    const chat = await chats
    .where('id', '==', chatId)
    .get()
    .then((query) => {
        return query.docs.map((doc) => {
            const chatData = doc.data();
            const chatRef = doc.ref;
            chatRef.update({
                recentMessage: newMessage
            });
            return chatData;
        });
    });
    if (!chat[0]){
        chats.add({
            id: chatId,
            chatters: [toId, fromId],
            recentMessage: newMessage,
            metaData: [
                 {
                    id: toId,
                    firstName: toName.firstName,
                    lastName: toName.lastName
                },
                {
                    id: fromId,
                    firstName: fromName.firstName,
                    lastName: fromName.lastName
                }
            ]
        });
    }
};

const Input = ({ toId, fromId, chatId, toName, fromName, addMessage }) => {
    const [message, setMessage] = useState('');

    const placeholder = "Write something...";

    const pressSend = (message) => {
        const newMessage = {
            body: message,
            time: new Date(),
            to: toId,
            from: fromId,
            chatId: chatId
        };
        setMessage('');
        addMessage(newMessage);   
        sendMessage(newMessage, toId, fromId, chatId, toName, fromName);
    };

    return (
        <View style={styles.container}>
            <View style={styles.inputContainer}>
                <TextInput style={styles.input} 
                    value={message} 
                    onChangeText={setMessage} 
                    placeholder={placeholder}/>
            </View>

            <TouchableOpacity style={styles.send} onPress={() => pressSend(message)} >
                <Text style={styles.sendText}>Send</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Input;

const styles = StyleSheet.create({
    send: {
        backgroundColor: 'purple',
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
        position: 'absolute',
        bottom: 0,
        paddingVertical: 5,
        borderTopWidth: 1,
        borderTopColor: 'grey',
    },
    inputContainer: {
        width: '70%'
    },
    input: {
        height: 40,
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 3,
        flexDirection: 'row',
        paddingHorizontal: 10
    },
    send: {
        backgroundColor: '#0A7BC7',
        height: 35,
        width: 45,
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    sendText: {
        color: 'white',
        fontWeight: 'bold',
    }
});