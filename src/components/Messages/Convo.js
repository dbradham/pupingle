import React, { useState, useEffect, useContext } from "react";
import { FlatList, View, StyleSheet } from 'react-native';
import CustomHeader from '../CustomHeader';
import Input from './Input';
import Message from './Message';
import firebase from '../../../firebase';
import { Context } from '../../context/AuthContext';

const getChatId = (uid, oid) => {
    if (uid > oid) {
        return uid + oid;
    } else {
        return oid + uid;
    }
};

const getMessages = async (chatId, setMessages) => {
    const chats = firebase.firestore().collection('messages');

    let messages = await chats
    .where('chatId', '==', chatId)
    .get()
    .then((query) => {
        return query.docs.map((doc) => {
            return doc.data();
        });
    });

    messages = messages.sort((a, b) => {return a.time.seconds > b.time.seconds});

    setMessages(messages);
};

const Convo = (props) => {
    const [chat, setChat] = useState(props.navigation.state.params.chat);
    const { state } = useContext(Context);
    const user = state.user;
    const uid = user.uid;

    const [messages, setMessages] = useState([]);

    const oid = chat.chatters[0] == uid ? chat.chatters[1] : chat.chatters[0];
    const chatId = getChatId(uid, oid);

    useEffect(() =>  {
        getMessages(chatId, setMessages);
    }, []);

    const addMessage = (message) => {
        setMessages([...messages, message]);
    }

    const metaData = chat.metaData;

    let toName = '';
    let fromName = '';
    if (metaData[0].id == uid) {
        fromName = metaData[0];
        toName = metaData[1];
    } else if (metaData[1].id == uid) {
        fromName = metaData[1];
        toName = metaData[0];
    }

    const nameOfRecipient = toName.firstName;// + " " + toName.lastName;
    return <>
        <CustomHeader title={nameOfRecipient} navigation={props.navigation} />
            <View style={styles.messagesContainer}>
                <FlatList
                    data={messages}
                    keyExtractor={function (item) {
                        return item.time.nanoseconds + "";
                    }}
                    renderItem={function ({ item }) {
                        return (
                            <Message 
                                side={item.from == uid ? 'right' : 'left'} 
                                message={item.body}
                            />
                        )
                    }}
                />
            </View>

            <Input
                firebase={firebase}
                toId={oid}
                fromId={uid}
                chatId={chatId}
                toName={toName}
                fromName={fromName}
                addMessage={addMessage} />
    </>
}
export default Convo;

const styles = StyleSheet.create({
    messagesContainer: {
        height: '85%',
        paddingBottom: 100
    },
});