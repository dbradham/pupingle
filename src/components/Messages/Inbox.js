import React, { useContext, useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    TextInput,
    Dimensions
} from 'react-native';
import CustomHeader from '../CustomHeader';
import MessageTile from './MessageTile';
import { Context } from  '../../context/AuthContext';
import firebase from '../../../firebase';

const getChats = (uid, setChats, setActiveChats) => {
    firebase
    .firestore()
    .collection('chats')
    .where('chatters', 'array-contains', uid)
    .onSnapshot((snapshot) => {
        const dbChats = snapshot.docs.map((doc) => {
            return doc.data();
        });
        setChats(dbChats);
        setActiveChats(dbChats);
    });
};

const getChatsForSearch = (text, chats) => {
    return chats.map((chat) => {
        let thisOne = false;
        chat.metaData.map((datum) => {
            const name = datum.firstName.toLowerCase() + 
            ' ' + datum.lastName.toLowerCase();

            if (name.includes(text.toLowerCase())) {
                thisOne = true;
            }
        });
        if (thisOne) {
            return chat;
        }
    }).filter((e) => { return e != null });
};

const Inbox = (props) => {
    const { state } = useContext(Context);
    const user = state.user;
    const uid = user.uid;
    const [chats, setChats] = useState([]);
    const [activeChats, setActiveChats] = useState([]);

    useEffect(() => {
        getChats(uid, setChats, setActiveChats);
    }, []);

    const searchPlaceholder = 'Search...';

    const searchChats = (name, chats) => {
        const chatsForSearch = getChatsForSearch(name, chats);
        setActiveChats(chatsForSearch);
    };

    return (
        <View style={styles.messageContainer}>
            <CustomHeader title="Inbox" navigation={props.navigation} />
            <TextInput onChangeText={name => searchChats(name, chats)}
             placeholder={searchPlaceholder} 
             style={styles.input} />
            <FlatList
                // contentContainerStyle
                data={activeChats}
                renderItem={({ item }) => (
                    <MessageTile
                        chat={item}
                        navigation={props.navigation}
                        uid={uid} />
                )}
                keyExtractor={item => item.id}
                style={styles.list}
            />
        </View>
    );
};

export default Inbox;

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
    list: {
        marginTop: 5,
    },
    messageContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    input: {
        height: 45,
        width: width * 0.95,
        borderRadius: width * 0.15,
        borderWidth: 2,
        borderColor: '#888888',
        flexDirection: 'row',
        paddingHorizontal: 10,
        marginLeft: 7.5,
        marginTop: 5,
    },
});