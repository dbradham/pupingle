import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const MessageTile = ({ navigation, chat, uid }) => {
    const recentMessage = chat.recentMessage;
    
    const time = recentMessage.time;
    const seconds = time.seconds;
    const d = new Date(seconds * 1000);
    const now = new Date();
    const days = (now - d) / 1000 / 60 / 60 / 24;
    let timeOfDay = '';
    let m = '';
    if (days >= 1) {
        timeOfDay = parseInt(days) + 'd'
    } else {
        m = 'AM';
        let hours = d.getHours();
        if (hours >= 13) {
            hours = hours - 12;
            m = 'PM'
        }
        const minutes = d.getMinutes();
        timeOfDay = hours + ':' + minutes;
    }


    const recentMessageBody = recentMessage.body.length > 36 ? 
    recentMessage.body.substring(0, 36) + '...' 
    :
    recentMessage.body;

    const metaData = chat.metaData;

    let toName = '';
    let fromName = '';
    if (metaData[0].id == uid) {
        fromName = metaData[0];
        toName = metaData[1];
    } else if (metaData[1].id == uid) {
        fromName = metaData[1];
        toName = metaData[0];
    }

    const nameOfRecipient = toName.firstName + " " + toName.lastName;

    return (
        <TouchableOpacity style={styles.container} onPress={() => {
            navigation.navigate("Convo", { chat })
        }} >
            <View style={styles.upperRow}>
                <Text style={styles.name}>{nameOfRecipient}</Text>
                <Text style={styles.time}>{timeOfDay} {m}</Text>
            </View>
            <Text style={styles.preview}>{recentMessageBody}</Text>
        </TouchableOpacity>
    );
};

export default MessageTile;

const styles = StyleSheet.create({
    name: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 10,
    },
    preview: {
        fontSize: 18,
        marginLeft: 20,
        marginTop: 15,
        marginRight: 20,
    },
    container: {
        alignSelf: 'stretch',
        height: 75,
        color: 'red',
        flex: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        margin: 2,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        paddingBottom: 10,
    },
    messageTileText: {
        fontWeight: 'bold',
        fontSize: 16
    },
    upperRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 10,
    },
    time: {
        marginTop: 10,
    }
});