import React from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";
import Picture from "./Picture";
import ImageFooter from "./ImageFooter";
import ImageHeader from "./ImageHeader";
import { TouchableOpacity } from "react-native-gesture-handler";

const Tile = ({ user, name, pic, navigation, dog, id }) => {
  return (
    <View style={styles.container}>
      <ImageHeader dog={dog} user={user} />
      <View>
        <Picture src={pic} />
      </View>
      <ImageFooter navigation={navigation} contentId={44} />
      <TouchableOpacity
        onPress={() => navigation.navigate("DogProfile", { dog: dog })}
      >
        <Text style={styles.name}>{name}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Tile;

const size = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    width: size,
    backgroundColor: "white",
    borderTopWidth: 3,
    borderTopColor: "#eeeeee"
  },
  name: {
    color: "#3768a3",
    fontSize: 24,
    fontWeight: "bold",
    marginLeft: 5
  }
});
