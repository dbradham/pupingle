import React from "react";
import { View } from "react-native";
import CustomHeader from '../CustomHeader';

const AdvancedSettings = (props) => {
    return (
        <View style={{ flex: 1, backgroundColor: "blue" }}>
            <CustomHeader navigation={props.navigation} />
            <Button
                title="Back"
                onPress={() => props.navigation.navigate("Settings")}
            />
        </View>
    );
}
export default AdvancedSettings;