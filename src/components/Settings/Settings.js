import React, { useContext } from "react";
import { Button, View } from "react-native";
import CustomHeader from '../CustomHeader';
import { Context } from '../../context/AuthContext';


const Settings = (props) => {
    const { state, signout } = useContext(Context);

    return (
        <View style={{ flex: 1, backgroundColor: "black" }}>
            <CustomHeader navigation={props.navigation} />
            <Button
                title="Advanced Settings"
                onPress={() => props.navigation.navigate("Advanced Settings")}
            />
            <Button
                title="Sign Out"
                onPress={() => signout()}
            />
        </View>
    );
}

export default Settings;