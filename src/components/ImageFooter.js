import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Modal,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Button,
  FlatList,
  TextInput
} from "react-native";
import CustomIcon from "./CustomIcon";
import ImageHeader from "./ImageHeader";
import firebase from "../../firebase";
import { MaterialIcons } from "@expo/vector-icons";

const fs = firebase.firestore();

const ImageFooter = props => {
  const dogTounge = require("../assets/images/dogTounge.png");
  const megaphone = require("../assets/images/megaphone.png");
  const wag = require("../assets/images/wag.png");
  const pin = require("../assets/images/location.png");

  const collectionName = `${props.type}Likes`;

  const [likeCount, setLikeCount] = useState(0);

  useEffect(() => {
    const id = props.contentId;
    fs.collection(collectionName)
      .where("id", "==", id)
      .get()
      .then(result => {
        setLikeCount(result.size);
      });
  }, []);

  const like = id => {
    let model = {
      time: new Date(),
      id: id,
      userId: props.userId
    };
    fs.collection(collectionName)
      .where("id", "==", id)
      .where("userId", "==", props.userId)
      .limit(1)
      .get()
      .then(result => {
        // Like already exists
        if (result.size) {
          fs.doc(`/${collectionName}/${result.docs[0].id}`).delete();
          const newLikeCount = likeCount - 1;
          setLikeCount(newLikeCount);
        } else {
          fs.collection(collectionName).add(model);
          const newLikeCount = likeCount + 1;
          setLikeCount(newLikeCount);
        }
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.leftGrouping}>
        <CustomIcon source={pin} />
        <Text>2.5mi</Text>
      </View>
      <View style={styles.rightGrouping}>
        <View style={styles.icon}>
          <CustomIcon
            style={styles.dogTounge}
            source={dogTounge}
            onPress={() => like(props.contentId)}
          />
          <Text>{likeCount}</Text>
        </View>
        <View style={styles.icon}>
          <CustomIcon
            source={megaphone}
            onPress={() => {
              props.navigation.navigate("Comments", props);
            }}
          />
          <Text>22</Text>
        </View>
        <View style={styles.icon}>
          <CustomIcon source={wag} />
          <Text>3</Text>
        </View>
      </View>
    </View>
  );
};
export default ImageFooter;
const deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    height: 65,
    backgroundColor: "white",
    display: "flex",
    flexDirection: "row",
    width: deviceWidth,
    justifyContent: "space-between"
  },
  leftGrouping: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 10,
    justifyContent: "center"
  },
  rightGrouping: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  icon: {
    marginLeft: 7.5,
    display: "flex",
    flexDirection: "column",
    height: 65,
    justifyContent: "center",
    alignItems: "center"
  },
  dogTounge: {
    height: 25
  }
});
