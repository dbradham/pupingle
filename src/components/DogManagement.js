import React from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import DogCreation from './DogCreation';

const DogManagement = ({ uid, navigation }) => {
    const src4 = require('../assets/images/dog4.png');

    return (
        <View>
            <DogCreation iconStyle={styles.icon} iconSize={35} navigation={navigation} />
            <TouchableOpacity>
                <Image style={styles.dog} source={src4} />
            </TouchableOpacity>
        </View>
    );
};

export default DogManagement;

const styles = StyleSheet.create({
    dog: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 3,
        borderColor: '#707070',
        marginHorizontal: 10,
    },
    icon: {
        color: 'blue',
        margin: 0,
        padding: 0,
        position: 'relative',
        top: 15,
    },
});