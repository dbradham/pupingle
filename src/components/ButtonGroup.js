import React from 'react';
import { View, StyleSheet } from 'react-native';
import { ButtonGroup } from 'react-native-elements';

const CustomButtonGroup = (props) => {
    return (
        <View style={styles.container}>
            <ButtonGroup
                onPress={() => props.onPress(props.offMode, props.mode)}
                selectedIndex={props.index}
                buttons={props.buttons}
                containerStyle={props.style} />
        </View>
    );
};

export default CustomButtonGroup;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        marginBottom: 15,
    },
});