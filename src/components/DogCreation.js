import React, { useState, useContext } from 'react';
import { Ionicons } from "@expo/vector-icons";
import {
    Modal,
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Platform
} from 'react-native';
import { Item, Input, Label } from "native-base";
import firebase from '../../firebase';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import ModalDropdown from 'react-native-modal-dropdown';
import ToggleSwitch from 'toggle-switch-react-native';
import ButtonGroup from './ButtonGroup';
import { Context } from '../context/AuthContext';

const validation = ({ image, uid, name, breed, mode, month, day, year, setError }) => {
    if (image == null) {
        setError('Please select an image for your dog to complete the sign up process');
        return false;
    }

    if (name == null) {
        setError('Please enter the name of your dog to complete the sign up process');
        return false;
    }

    if (mode == null) {
        setError('Please select a gender for your dog to complete the sign up process');
        return false;
    }

    if (year == null || month == null || day == null) {
        setError('Please enter a complete birthday for you dog to complete the sign up process');
        return false;
    }

    if (breed == null) {
        setError('Please select a breed for your dog to complete the sign up process');
        return false;
    }

    return true;
};

const _getPhotoLibrary = async (setImage) => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3]
           });
        if (!result.cancelled) {
            setImage(result.uri);
        }   
    }
}

const uploadImage = async (image, name) => {
    console.log('extract function', image, name);
    const response = await fetch(image);
    const blob = await response.blob();

    var ref = firebase.storage().ref().child("DogAvatars/" + name);
    return ref.put(blob);
}

const DogCreation = (props) => {
    const quitIcon = Platform.OS === 'ios'
    ? 'ios-close-circle' : 'md-close-circle';

    const addIcon = Platform.OS === 'ios'
        ? 'ios-add-circle': 'md-add-circle';

    const checkIcon = Platform.OS === 'ios'
        ? 'ios-checkmark-circle' : 'md-checkmark-circle';

    const buttonList = ['Female', 'Male'];

    const years = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009,
    2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2018, 2019, 2020].reverse();

    const months = ['January', 'February', 'March', 'April', 'May', 'June', 
    'July', 'August', 'September', 'October', 'November', 'December'];

    const [mode, setMode] = useState('female');
    const [offMode, setOffMode] = useState('male');
    const [bgIndex, setBgIndex] = useState(0);
    const [error, setError] = useState('');
    const [modalVisible, setModalVisible] = useState(false);

    const [name, setName] = useState('');
    const [breed, setBreed] = useState('');
    const [month, setMonth] = useState('');
    const [day, setDay] = useState('');
    const [year, setYear] = useState('');
    const [image, setImage] = useState('');

    const changeMode = (mode, offMode) => {
        let bgIndex = 0;
        if (mode != 'female') {
            bgIndex = 1;
        }
        setMode(mode);
        setBgIndex(bgIndex);
        setOffMode(offMode);
    }

    const { state, createDog } = useContext(Context);
    const user = state.user;
    const uid = user.uid;
    const validateAndCreate = (dog) => {
        const isValid = validation(dog, setError);
        if (isValid) {
            const { image, uid, name, breed, mode, month, day, year } = dog;
            const birthday = new Date(year, month, day);
            const firestoreDog = {
                uid,
                name,
                breed,
                gender: mode,
                birthday
            }
            createDog(firestoreDog);
            const imageUpload = async (image, name) => {
                await uploadImage(image, name);
            };
            imageUpload(image, name).then(() => {
                setModalVisible(false);
                props.navigation.navigate("DogProfile", { dog: firestoreDog, user: user });
            });
        }
    };

    const subView = <View style={styles.subViewContainer}>
            <Ionicons onPress={() => {
                    setModalVisible(false);
                }} size={35} name={quitIcon} style={styles.quitIcon} />  

            <Text style={styles.header}>
                Add your Dog to Puppingle!
            </Text>

            <Text style={{color: 'red'}}>{error}</Text>

            <TouchableOpacity style={styles.imageUploadOption}
                            onPress={() => _getPhotoLibrary(setImage)}>
                <Text style={styles.imageUploadText}>
                    Select Profile Pic
                </Text>
            </TouchableOpacity>
            
            <View style={styles.inputs}>

                <View style={styles.inputLine}>
                    <Text style={styles.rowHeader}>Name</Text>

                    <Item floatingLabel style={styles.item}>
                        <Label style={styles.label}>Your dog's name</Label>

                        <Input
                            value={name}
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={name => setName(name)} />
                    </Item>
                </View>

                <View style={styles.inputLine}>
                    <Text style={styles.rowHeader}>Gender</Text>

                    <ButtonGroup buttons={buttonList} 
                        index={bgIndex} 
                        onPress={changeMode}
                        offMode={offMode} 
                        mode={mode}
                        style={styles.buttonGroup}/>

                    {/* <Text style={styles.genderText}>Female</Text>
                    
                    <ToggleSwitch
                        isOn={this.state.genderState}
                        onColor="#0A7BC7"
                        offColor="#cc5500"
                        size="large"
                        onToggle={() => this.setState({ genderState: !this.state.genderState })}
                    />
                    
                    <Text style={styles.genderText}>Male</Text> */}
                </View>

                <View style={styles.inputLine}>
                    <Text style={styles.rowHeader}>Birthday</Text>

                    <View style={styles.birthdayInputs}>
                        <ModalDropdown defaultValue="Month" style={styles.birthdayDropDown}
                            options={months}
                            dropdownStyle={styles.innerDropDown}
                            dropdownTextStyle={styles.dropdownTextStyle}
                            textStyle={styles.textStyle}
                            onSelect={(index, value) => setMonth(index)} />

                        <ModalDropdown defaultValue="Day" style={styles.birthdayDropDown}
                            options={[...Array(32).keys()]}
                            dropdownStyle={styles.innerDropDown}
                            dropdownTextStyle={styles.dropdownTextStyle}
                            textStyle={styles.textStyle}
                            onSelect={(index, value) => setDay(value)} />

                        <ModalDropdown defaultValue="Year" style={styles.birthdayDropDown}
                            options={years}
                            dropdownStyle={styles.innerDropDown}
                            dropdownTextStyle={styles.dropdownTextStyle}
                            textStyle={styles.textStyle}
                            onSelect={(index, value) => setYear(value)} />
                    </View>
                </View>

                <View style={styles.inputLine}>
                    <Text style={styles.rowHeader}>Breed</Text>

                    <ModalDropdown defaultValue="Breed" style={styles.bigDropDown}
                        options={['Golden Retriever', 'Greyhound']}
                        dropdownStyle={styles.innerDropDown}
                        dropdownTextStyle={styles.dropdownTextStyle}
                        textStyle={styles.textStyle}
                        onSelect={(index, value) => setBreed(value)} />
                </View>

                <TouchableOpacity style={styles.checkIconContainer} onPress={() => validateAndCreate({
                    image,
                    uid,
                    name,
                    breed,
                    mode,
                    month,
                    day,
                    year,
                    setError                    
                })}>
                        <Ionicons size={75} name={checkIcon} style={styles.checkIcon} />
                </TouchableOpacity>
            </View>  
        </View>;

    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View style={styles.innerModal}>
                    {subView}
                </View>
            </Modal>
            <TouchableOpacity style={props.iconContainerStyle} onPress={() => setModalVisible(true)}>
                <Ionicons name={addIcon} style={props.iconStyle} size={props.iconSize}/>
            </TouchableOpacity>
        </View>
    );
};

export default DogCreation;

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    innerModal: {
        flex: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
    },
    header: {
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 15,
        width: '90%',
        textShadowColor: '#0A7BC7',
        textShadowRadius: 2,
        // alignSelf: 'center',
    },
    item: {
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        width: deviceWidth * 0.6,
        borderRadius: deviceWidth * 0.05,
        color: '#0A7BC7',
        borderWidth: 1,
        borderColor: '#0A7BC7'
    },
    inputLine: {
        marginVertical: 10,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    label: {
        marginLeft: 10,
        color: '#0A7BC7'
    },
    subView: {
        marginHorizontal: deviceWidth * 0.025,
        backgroundColor: 'black',
        color: '#E53BF9',
        height: deviceHeight * 0.5,
        flex: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    subViewContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 0.05 * deviceHeight,
    },
    checkIcon: {
        color: '#0A7BC7',
    },
    checkIconContainer: {
        alignSelf: 'center',
    },
    imageUploadText: {
        margin: 15,
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold',
    },
    imageUploadOption: {
        height: deviceWidth * 0.5,
        width: deviceWidth * 0.5,
        borderRadius: deviceWidth * 0.3,
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'black',
    },
    genderRow: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
    },
    textStyle: {
        fontSize: 18,
        color: 'black'
    },
    rowHeader: {
        fontSize: 20,
        color: 'black',
        fontWeight: 'bold',
    },
    dropdownTextStyle: {
        color: 'black',
    },
    birthdayDropDown: {
        height: 35,
        width: deviceWidth * 0.15,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'black',
        borderRadius: 5,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 7.5,
    },
    bigDropDown: {
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'black',
        width: '45%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    inputs: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: deviceWidth * 0.9,
    },
    birthdayInputs: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    genderText: {
        fontSize: 20,
    },
    quitIcon: {
        color: '#0A7BC7',
        alignSelf: 'flex-start',
    },
    buttonGroup: {
        height: 45, 
        width: 180
    }
});