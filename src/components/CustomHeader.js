import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet } from "react-native";

const pawPrint = require('../assets/images/pawIcon.png');
const searchIcon = require('../assets/images/search.png');

const CustomHeader = ({ navigation, title }) => {
    return (
        <View>
        <View style={styles.container}>
            <TouchableOpacity style={styles.pawIconContainer}
             onPress={() => navigation.openDrawer()}>
                <Image
                    style={styles.pawPrint}
                    source={pawPrint} />
            </TouchableOpacity>
            <View style={styles.navSection}>
                <Text style={styles.header}>{title}</Text>
            </View>
            <TouchableOpacity style={styles.alarmIconContainer}
            onPress={() => navigation.navigate("SearchScreen")}>
                <Image
                    style={styles.alarmIcon}
                    source={searchIcon} />
            </TouchableOpacity>
        </View>
    </View>
    );
};

export default CustomHeader;

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 2,
        height: 75,
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    pawPrint: {
        height: 32,
        width: 32,
    },
    pawIconContainer: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
    },
    alarmIcon: {
        width: 32,
        height: 32,
    },
    alarmIconContainer: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    icon: {
        marginLeft: 5,
    },
    navSection: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraContainer: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row',
    },
    camera: {
        marginRight: 5,
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#444444',
    }
});