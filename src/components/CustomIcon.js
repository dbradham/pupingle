import React from "react";
import { Image, TouchableOpacity } from "react-native";

const CustomIcon = props => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <Image style={props.style} source={props.source} />
    </TouchableOpacity>
  );
};

export default CustomIcon;
