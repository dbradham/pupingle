import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'

const Name = (props) => {
    const user = props.user;
    const name = props.user.dogs[props.user.activeDog].name;
    return (
        <View>
            <TouchableOpacity 
                onPress={() => props.navigation.navigate("Profile", { user: user } )} 
            >
                <Text
                    style={styles.item}
                >{name}
                </Text>
            </TouchableOpacity>
        </View>
    );
}
const styles = StyleSheet.create({
    item: {
        color: '#3768a3',
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: 5
    }
});
export default Name;