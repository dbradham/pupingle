import React from "react";
import { Button, View } from "react-native";
import CustomHeader from '../CustomHeader';

const Neighborhood = (props) => {
    return (
        <View style={{ flex: 1, backgroundColor: "black" }}>
            <CustomHeader navigation={props.navigation} />
            <Button
                title="To Secondary Neighborhood"
                onPress={() => props.navigation.navigate("Secondary")}
            />
        </View>
    );
}

export default Neighborhood;