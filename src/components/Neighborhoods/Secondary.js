import React, { Component } from "react";
import { View, Button } from "react-native";
import CustomHeader from '../CustomHeader';

export default class Secondary extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <CustomHeader navigation={this.props.navigation} />
                <Button
                    title="Back to neighborhoods main"
                    onPress={() => this.props.navigation.navigate("Neighborhood")}
                />
            </View>
        );
    }
}