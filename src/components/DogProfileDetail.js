import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Stat from './Stat';

const DogProfileDetail = ({ name, breed, age, gender }) => {
    return (
        <View>
            <View style={styles.dogInfoContainer}>
                <Text style={styles.dogName}>{name}</Text>
                <View style={styles.profileRow}>
                    <View style={styles.innerInfo}>
                        <Text style={styles.dogInfo}>{breed}</Text>
                        <Text style={styles.dogInfo}>{age}</Text>
                        <Text style={styles.dogInfo}>{gender}</Text>
                    </View>
                </View>               
            </View>
            <View style={styles.container}>
                <Stat name="Dawgs" value="415" />
                <Stat name="Buddies" value="350" />
                <Stat name="Photos" value="54" />
                <Stat name="Posts" value="700" />
            </View>
        </View>
    );
};

export default DogProfileDetail;

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10,
    },
    dogInfoContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    innerInfo: {
        display: 'flex',
        flexDirection: 'column',
    },
    dogInfo: {
        fontSize: 16,
        marginLeft: deviceWidth * 0.05,
    },
    dogName: {
        fontSize: 32,
        marginLeft: deviceWidth * 0.02,
        fontWeight: 'bold',
    },
    profileRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});