import React from "react";
import { View, Text, StyleSheet } from "react-native";

const ImageHeader = ({ user, dog}) => {
    const userName = user.firstName;
    const dogName = dog.name;
    return <View style={styles.container}>
        <View style={styles.rowContainer}>
            <View style={styles.avatar}></View>
            <View style={styles.textContainer}>
                <Text>{userName}</Text>
                <Text>{dogName}</Text>
            </View>
            <Text>.</Text>
        </View>
        <View style={styles.timeContainer}>
            <Text>12:31 PM on 12/20/2019</Text>
        </View>
    </View>
}
export default ImageHeader;

const styles = StyleSheet.create({
    container: {

    },
    timeContainer: {
        display: 'flex', 
        flexDirection: 'row', 
        justifyContent: 'flex-end' 
    },
    rowContainer: {
        display: 'flex', 
        flexDirection: 'row', 
    },
    textContainer: {
        display: 'flex',
        flexDirection: 'column',        
    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'black',
        margin: 5
    }
});