import { StyleSheet, Image } from 'react-native';
import React from 'react';

const Picture = ({ src }) => {
    return <Image
        style={styles.pic}
        source={{ uri: src }}
    />
}
export default Picture;

const styles = StyleSheet.create({
    pic: {
        width: 393,
        height: 325,
        borderWidth: 0.1,
        borderColor: '#3768a3',
    }
})