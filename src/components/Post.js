import React, { useEffect, useState } from "react";
import {
  View,
  ActivityIndicator,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Modal
} from "react-native";
import CustomIcon from "./CustomIcon";
import ImageFooter from "./ImageFooter";
import { TouchableOpacity } from "react-native-gesture-handler";
import firebase from "../../firebase";

const getDogProfileImage = dog => {
  const [dogProfileImage, setDogProfileImage] = useState([]);

  useEffect(() => {
    var storage = firebase.storage();
    var storageRef = storage.ref();

    let pathReference = storageRef.child("DogAvatars/" + dog.name);

    pathReference.getDownloadURL().then(url => {
      setDogProfileImage(url);
    });
  });

  return dogProfileImage;
};

const Post = ({ post, navigation, dogProfileImage }) => {
  if (post == null) {
    return <ActivityIndicator />;
  } else {
    const user = post.user;
    const dog = post.dog;
    const dogName = dog.name;

    if (dogProfileImage == null) {
      dogProfileImage = getDogProfileImage(dog);
    }

    const avatar = { url: dogProfileImage };

    const dots = require("../assets/images/dots.png");

    return (
      <View style={styles.container}>
        <View style={styles.upperPost}>
          <View style={styles.upperRow}>
            <Image style={styles.avatar} source={avatar} />
            <View style={styles.names}>
              <TouchableOpacity
                onPress={() => navigation.navigate("AccountProfile", { user })}
              >
                <Text style={styles.userName}>{post.user.firstName}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.navigate("DogProfile", { dog, user })}
              >
                <Text style={styles.dogName}>{dogName}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.rightColumn}>
            <CustomIcon source={dots} style={styles.dots} />
            <Text>12:00 on 12/1/20</Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.body}
          onPress={() => navigation.navigate("Post", { post, dogProfileImage })}
        >
          <Text style={styles.body}>{post.body}</Text>
        </TouchableOpacity>
        <ImageFooter
          navigation={navigation}
          contentId={post.id}
          userId={user.uid}
          type="post"
        />
      </View>
    );
  }
};

export default Post;

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomColor: "#eeeeee",
    borderBottomWidth: 2
  },
  body: {
    margin: width * 0.05
  },
  avatar: {
    height: 65,
    width: 65,
    borderRadius: 32.5,
    borderWidth: 2,
    borderColor: "black",
    marginLeft: 10
  },
  dots: {
    height: 10,
    width: 30,
    alignSelf: "flex-end"
  },
  upperPost: {
    marginTop: height * 0.05,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  upperRow: {
    display: "flex",
    flexDirection: "row"
  },
  userName: {
    fontWeight: "bold",
    fontSize: 24
  },
  dogName: {
    fontSize: 20,
    marginLeft: 5
  },
  names: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  rightColumn: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: 50,
    marginRight: 10
  }
});
