import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import DogCreation from './DogCreation';
import DogCircle from './DogCircle';

const DogScroll = ({ user, dogs, navigation }) => {
    let dogCircles = null;
    if (dogs.length > 0) {
        dogCircles = dogs.map((dog) => {
            return <DogCircle key={dog.name+dog.uid} dog={dog} user={user} navigation={navigation} />;
        });
    }

    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.circleContainer} horizontal={true}>
                {dogCircles}
                <DogCreation iconStyle={styles.icon}
                    iconContainerStyle={styles.iconContainer}
                    iconSize={115}
                    navigation={navigation} />
            </ScrollView>
        </View>
    );
};

export default DogScroll;

const styles = StyleSheet.create({
    container: {
        height: 120,
    },
    circleContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
});