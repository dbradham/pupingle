import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Dimensions } from 'react-native';
import { Item, Input } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import firebase from '../../firebase';

const createPostInFirestore = (post) => {
    const posts = firebase.firestore().collection('posts');
    async function add(post) {
      const dateTime = new Date();
      await posts.add({ ...post, dateTime });
    }
    add(post);
  };

const PostInput = ({ user, dog }) => {
    const [post, setPost] = useState('');
    const postIcon = Platform.OS === 'ios'
    ? 'ios-cloud-upload': 'md-cloud-upload';

    return (
        <Item style={styles.container}>
        <Input
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="What did your dog do today?"
            value={post}
            onChangeText={post => setPost(post)}
        />
        <TouchableOpacity onPress={() => {
            createPostInFirestore({ body: post, user, dog });
            setPost('');
        }}>
            <Ionicons size={40} name={postIcon} style={styles.postIcon} />
        </TouchableOpacity>
    </Item>
    );
};

export default PostInput;

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        width: deviceWidth * 0.9,
        marginVertical: 5,
        display: 'flex',
        flexDirection: 'row',
    },
    input: {
        borderWidth: 2,
        borderColor: '#32A6DB',
    },
    postContainer: {
        marginTop: 10,
        marginRight: 10,
        borderWidth: 2,
        borderColor: 'black',
        borderRadius: 10,
        maxWidth: deviceWidth * 0.40,
    },
    postIcon: {
        margin: 5,
        color: '#0A7BC7',
    },
});