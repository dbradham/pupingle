import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Image, 
    ActivityIndicator, 
} from 'react-native';

const AccountProfileUserDetail = ({ user, src, isCurrentUser }) => {
    if (user == '') {
        return <ActivityIndicator />;
    }

    return (
        <View style={styles.container}>
            <View>
                <Image style={styles.avatar}
                    source={src} />
            </View>
            <View>
                <Text style={styles.name}>
                    {user.firstName} {user.lastName}
                </Text>
            </View>
            <View style={styles.statsContainer}>
                <View style={styles.statColumn}>
                    <Text style={styles.statNum}>
                        200
                    </Text>
                    <Text style={styles.statText}>
                        Dawgs
                    </Text>
                </View>
                <View style={styles.statColumn}>
                    <Text style={styles.statNum}>
                        {user.buddies.length}
                    </Text>
                    <Text style={styles.statText}>
                        Buddies
                    </Text>
                </View>
                <View style={styles.statColumn}>
                    <Text style={styles.statNum}>
                        54
                    </Text>
                    <Text style={styles.statText}>
                        Photos
                    </Text>
                </View>
            </View>
        </View>
    );
};

export default AccountProfileUserDetail;

const colors = require('../static.json');

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '85%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 10,
        paddingTop: 30,
        paddingBottom: 15,
        alignSelf: 'center',
        shadowColor: 'black',
        shadowRadius: 10
    },
    avatar: {
        height: 150,
        width: 150,
        borderRadius: 75,
        borderWidth: 6,
        borderColor: colors.lightGrey,
    },
    name: {
        fontSize: 35,
        fontFamily: 'sf-rounded-pro-bold',
        color: colors.darkGrey,
        marginTop: 10
    },
    statsContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
    },
    statColumn: {
        display: 'flex',
        marginHorizontal: 10,
        flexDirection: 'column',
        alignItems: 'center',
    },
    statText: {
        color: colors.lightGrey,
        fontSize: 16,
        fontFamily: 'sf-rounded-pro-thin'
    },
    statNum: {
        color: colors.lightGrey,
        fontSize: 16,
        fontFamily: 'sf-rounded-pro-regular'
    },
});