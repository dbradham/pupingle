import React from 'react';
import { FlatList } from 'react-native';
import Tile from './Tile';

const Timeline = ({ navigation }) => {
    const data = [
        {
            name: 'Kawhi',
            pic: 'https://www.citynews1130.com/wp-content/blogs.dir/sites/9/2019/06/file5-1.jpeg',
            caption: '',
            neighborhood: 'Westlake',
            user: {
                firstName: 'john', 
                lastName: 'doe',
                email: 'johndoe@gmail.com',
                id: 1234,
                activeDog: 0
            },
            dog: {
                birthday: {
                    seconds: new Date(2018, 5, 7).getTime() / 1000
                },
                breed: 'Bulldog lookin thing',
                name: 'Max',
                gender: 'male',
            }
        },
        {
            name: 'Landry',
            pic: 'https://boygeniusreport.files.wordpress.com/2016/11/puppy-dog.jpg?quality=98&strip=all&w=782',
            caption: '',
            neighborhood: 'Riverside',
            user: {
                firstName: 'john', 
                lastName: 'doe',
                email: 'johndoe@gmail.com',
                id: 1234,
                activeDog: 0
            },
            dog: {
                birthday: {
                    seconds: new Date(2016, 5, 7).getTime() / 1000
                },
                breed: 'golden retriever',
                name: 'Greg',
                gender: 'male',
            },
        },
        {
            name: 'Lou',
            pic: 'https://cdn.akc.org/content/article-body-image/lab_puppy_dog_pictures.jpg',
            caption: '',
            neighborhood: 'Riverside',
            user: {
                firstName: 'john', 
                lastName: 'doe',
                email: 'johndoe@gmail.com',
                id: 1234,
                activeDog: 0
            },
            dog: {
                birthday: {
                    seconds: new Date(2017, 5, 7).getTime() / 1000
                },
                breed: 'golden retriever',
                name: 'Charles',
                gender: 'male',
            },
        },
        {
            name: 'Paul',
            pic: 'https://i.pinimg.com/originals/8c/7a/e2/8c7ae28680cd917192d6de5ef3d8cd7f.jpg',
            caption: '',
            neighborhood: 'Hyde Park',
            user: {
                firstName: 'john', 
                lastName: 'doe',
                email: 'johndoe@gmail.com',
                id: 1234,
                activeDog: 0
            },
            dog: {
                birthday: {
                    seconds: new Date(2019, 5, 7).getTime() / 1000
                },
                breed: 'golden retriever',
                name: 'young dog',
                gender: 'male',
            }
        },
    ];

    return (
        <FlatList
            vertical
            showsVerticalScrollIndicator={false}
            data={data}
            renderItem={({ item }) => (
                <Tile
                    user={item.user}
                    dog={item.dog}
                    id={item.id}
                    name={item.name}
                    caption={item.caption}
                    neighborhood={item.neighborhood}
                    navigation={navigation}
                    pic={item.pic}
                />
            )}
            keyExtractor={item => item.pic}
        />
    );
};

export default Timeline;