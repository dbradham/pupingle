import React, { useContext } from "react";
import { View, Text, StyleSheet } from "react-native";
import { DrawerItems } from "react-navigation";
import CustomDrawerItem from './CustomDrawerItem';
import DogManagement from './DogManagement';
import DrawerNavigatorUserDetail from './DrawerNavigatorUserDetail';
import { Context } from '../context/AuthContext';

const CustomerDrawerNavigator = (props) => {
    const { state } = useContext(Context);
    const user = state.user;

    const resetIndex = (name, focused) => {
        const routeName = name.routeName;

        if (routeName == 'SettingsTabNavigator') {
            props.navigation.navigate("Settings");
        } else if (routeName == 'NotificactionsTabNavigator') {
            props.navigation.navigate("NotificationList");
        } else if (routeName == 'NeighborhoodTabNavigator') {
            props.navigation.navigate("Neighborhood");
        } else if (routeName == 'MessagesTabNavigator') {
            props.navigation.navigate("Inbox");
        } else if (routeName == "SearchTabNavigator") {
            props.navigation.navigate("SearchScreen");
        }
    }

    const name = user.firstName + ' ' + user.lastName;
    const uid = user.uid;

    const buddyCount = user.buddies.length; 

    return (
        <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.header}>{name}</Text>
                </View>

                <DrawerNavigatorUserDetail photos={0} dawgs={0} buddies={buddyCount} />

                <View style={styles.drawerContainer}>
                    <DrawerItems
                        {...props}
                        onItemPress={({ route, focused }) => {
                            resetIndex(route, focused);
                        }}
                        getLabel={scene => <CustomDrawerItem scene={scene} {...props} />}
                        renderIcon={() => { }}
                        activeBackgroundColor={"white"}
                        activeTintColor={"white"}
                    />
                </View>

                <View style={styles.footer}>
                    <DogManagement uid={uid} navigation={props.navigation} />
                </View>
            </View>
    );
};

export default CustomerDrawerNavigator;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1
    },
    header: {
        color: '#707070',
        fontWeight: 'bold',
        fontSize: 24
    },
    headerContainer: {
        display: 'flex',
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 22,
        flex: 0.5
    },
    drawerContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
        flex: 3
    },
    icons: {
        width: 60
    },
    signout: {
        display: 'flex',
        backgroundColor: 'red',
        flexDirection: 'row',
        width: 50,
        alignItems: 'stretch'
    },
    signoutIcon: {
        color: 'red',
    },
    footer: {
        display: 'flex',
        justifyContent: 'flex-end',
        flexDirection: 'column',
        alignItems: 'flex-end',
        margin: 15,
        flex: 1,
        marginBottom: 25,
    },
});