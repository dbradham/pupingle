import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import Stat from './Stat';

const DrawerNavigatorUserDetail = ({ dawgs, buddies, photos }) => {
    const avatar = require('../assets/images/sampleAvatar.png');

    return (
        <View style={styles.container}>
            <Image style={styles.avatar} source={avatar} />
            <Stat value={dawgs} name="Dawgs" />
            <Stat value={buddies} name="Buddies" />
            <Stat value={photos} name="Photos" />
        </View>
    );
};

export default DrawerNavigatorUserDetail;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    column: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    text: {
        color: '#707070',
        fontWeight: 'bold',
    },
    avatar: {
        height: 65,
        width: 65,
        borderRadius: 32.5,
        borderWidth: 2,
        borderColor: 'black',
    },
})