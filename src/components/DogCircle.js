import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Image, StyleSheet, ActivityIndicator } from 'react-native';
import firebase from '../../firebase';

const getDogProfileImage = (dog) => {
    const [dogProfileImage, setDogProfileImage] = useState('');

    useEffect(() => {
        var storage = firebase.storage();
        var storageRef = storage.ref();
    
        let pathReference = storageRef.child("DogAvatars/" + dog.name);
    
        pathReference.getDownloadURL().then((url) => {
            setDogProfileImage(url);
        });
    }, []);

    return dogProfileImage;
}

const DogCircle = ({ dog, user, navigation }) => {        
    const dogProfileImage = getDogProfileImage(dog);

    const img = dogProfileImage == '' ? 
    <ActivityIndicator size="large" style={styles.dog} /> 
    : 
    <Image style={styles.dog} source={{ uri: dogProfileImage }} />;

    return (
        <TouchableOpacity onPress={() => navigation.navigate("DogProfile", { dog, user })}>
            {img}
        </TouchableOpacity>
    );
}

export default DogCircle;

const styles = StyleSheet.create({
    dog: {
        height: 100,
        width: 100,
        borderRadius: 50,
        borderWidth: 3,
        borderColor: '#EF8912',
        marginHorizontal: 10,
    },
})