import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Stat = ({ name, value }) => {
    return (
        <View style={styles.column}>
            <Text style={styles.text}>{value}</Text>
            <Text style={styles.text}>{name}</Text>
        </View>
    );
};

export default Stat;

const styles = StyleSheet.create({
    column: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    text: {
        fontWeight: 'bold',
        color: '#454545',
        fontSize: 20,
    },
});