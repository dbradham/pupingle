import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Main from '../../components/Settings/Settings';
import Secondary from "../../components/Settings/AdvancedSettings";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const HelpNavigator = createStackNavigator(
    {
        Main: {
            navigationOptions: {
                header: null
            },
            screen: Main
        },

        Secondary: {
            navigationOptions: {
                header: null
            },
            screen: Secondary
        }
    },
    config
);

HelpNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'help',
    tabBarLabel: 'Help & Support'
});

export default HelpNavigator;