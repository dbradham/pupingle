import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Neighborhood from '../../components/Neighborhoods/Neighborhood';
import Secondary from "../../components/Neighborhoods/Secondary";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const SettingsNavigator = createStackNavigator(
    {
        Neighborhood: {
            navigationOptions: {
                header: null
            },
            screen: Neighborhood
        },

        Secondary: {
            navigationOptions: {
                header: null
            },
            screen: Secondary
        }
    },
    config
);

SettingsNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'settings',
    tabBarLabel: 'Settings'
});

export default SettingsNavigator;