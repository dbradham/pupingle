import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import SearchScreen from '../../screens/SearchScreen';
import Secondary from "../../components/Neighborhoods/Secondary";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const SearchNavigator = createStackNavigator(
    {
        SearchScreen: {
            navigationOptions: {
                header: null
            },
            screen: SearchScreen
        },

        Secondary: {
            navigationOptions: {
                header: null
            },
            screen: Secondary
        }
    },
    config
);

SearchNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'search',
    tabBarLabel: 'Search'
});

export default SearchNavigator;