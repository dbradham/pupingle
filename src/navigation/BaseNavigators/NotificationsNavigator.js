import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import NotificationList from '../../components/Notifications/NotificationList';
import SpecificNotification from "../../components/Notifications/SpecificNotification";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const SearchNavigator = createStackNavigator(
    {
        NotificationList: {
            navigationOptions: {
                header: null
            },
            screen: NotificationList
        },

        SpecificNotification: {
            navigationOptions: {
                header: null
            },
            screen: SpecificNotification
        }
    },
    config
);

SearchNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'posts',
    tabBarLabel: 'Posts'
});

export default SearchNavigator;