import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Inbox from '../../components/Messages/Inbox';
import Convo from "../../components/Messages/Convo";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const MessagesNavigator = createStackNavigator(
    {
        Inbox: {
            navigationOptions: {
                header: null
            },
            screen: Inbox
        },

        Convo: {
            navigationOptions: {
                header: null
            },
            screen: Convo
        }
    },
    config
);

MessagesNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'messages',
    tabBarLabel: 'Messages'
});

export default MessagesNavigator;