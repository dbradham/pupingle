import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Main from '../../components/Settings/Settings';
import Secondary from "../../components/Settings/AdvancedSettings";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const GroupsNavigator = createStackNavigator(
    {
        Main: {
            navigationOptions: {
                header: null
            },
            screen: Main
        },

        Secondary: {
            navigationOptions: {
                header: null
            },
            screen: Secondary
        }
    },
    config
);

GroupsNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'groups',
    tabBarLabel: 'Groups'
});

export default GroupsNavigator;