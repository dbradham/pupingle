import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Settings from '../../components/Settings/Settings';
import AdvancedSettings from "../../components/Settings/AdvancedSettings";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const SettingsNavigator = createStackNavigator(
    {
        Settings: {
            navigationOptions: {
                header: null
            },
            screen: Settings
        },

        AdvancedSettings: {
            navigationOptions: {
                header: null
            },
            screen: AdvancedSettings
        }
    },
    config
);

SettingsNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarTestID: 'settings',
    tabBarLabel: 'Settings & Privacy'
});

export default SettingsNavigator;