import { createBottomTabNavigator } from 'react-navigation';
import Timeline from "../TabNavigation/Tabs/TimelineNavigator";
import Calendar from '../TabNavigation/Tabs/CalendarNavigator';
import Profile from '../TabNavigation/Tabs/ProfileNavigator';
import Posts from '../TabNavigation/Tabs/PostsNavigator';
import Search from '../TabNavigation/Tabs/CalendarNavigator';
import Groups from '../BaseNavigators/GroupsNavigator';
import CustomTabButton from '../../components/CustomTabButton';

const GroupsTabNavigator = createBottomTabNavigator(
    {
        Groups,
        Timeline,
        Calendar,
        Profile,
        Posts,
        Search,
    },
    {
    Options: ({ navigation }) => ({
        tabBarButtonComponent: CustomTabButton
    })
    }
);

export default GroupsTabNavigator;