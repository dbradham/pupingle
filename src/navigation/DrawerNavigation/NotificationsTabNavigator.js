import { createBottomTabNavigator } from 'react-navigation';
import Timeline from "../TabNavigation/Tabs/TimelineNavigator";
import Calendar from '../TabNavigation/Tabs/CalendarNavigator';
import Search from '../TabNavigation/Tabs/SearchNavigator';
import Profile from '../TabNavigation/Tabs/ProfileNavigator';
import Posts from '../TabNavigation/Tabs/PostsNavigator';
import Notifications from '../BaseNavigators/NotificationsNavigator';
import CustomTabBar from '../../components/CustomTabBar';

const SearchTabNavigator = createBottomTabNavigator(
    {
        Notifications,
        Timeline,
        Calendar,
        Profile,
        Posts,
        Search,
    },
    {
        tabBarComponent: CustomTabBar,
    }
);

export default SearchTabNavigator;