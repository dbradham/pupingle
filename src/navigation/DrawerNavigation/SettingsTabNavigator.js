import { createBottomTabNavigator } from 'react-navigation';

import Timeline from "../TabNavigation/Tabs/TimelineNavigator";
import Calendar from '../TabNavigation/Tabs/CalendarNavigator';
import Search from '../TabNavigation/Tabs/SearchNavigator';
import Posts from '../TabNavigation/Tabs/PostsNavigator';
import Profile from '../TabNavigation/Tabs/ProfileNavigator';

import Settings from '../BaseNavigators/SettingsNavigator';

import CustomTabBar from '../../components/CustomTabBar';

const SettingsTabNavigator = createBottomTabNavigator(
    {
        Settings,
        Timeline,
        Calendar,
        Profile,
        Posts,
        Search,
    },
    {
        tabBarComponent: CustomTabBar,
    }
);

export default SettingsTabNavigator;