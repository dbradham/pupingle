import { createBottomTabNavigator } from 'react-navigation';

import Timeline from "../TabNavigation/Tabs/TimelineNavigator";
import Calendar from '../TabNavigation/Tabs/CalendarNavigator';
import Search from '../TabNavigation/Tabs/SearchNavigator';
import Posts from '../TabNavigation/Tabs/PostsNavigator';
import Profile from '../TabNavigation/Tabs/ProfileNavigator';

import Neighborhood from '../BaseNavigators/NeighborhoodNavigator';

import CustomTabBar from '../../components/CustomTabBar';

const NeighborhoodTabNavigator = createBottomTabNavigator(
    {
        Neighborhood,
        Timeline,
        Calendar,
        Profile,
        Posts,
        Search,
    },
    {
        tabBarComponent: CustomTabBar,
    }
);

export default NeighborhoodTabNavigator;