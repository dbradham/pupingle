import { createBottomTabNavigator } from 'react-navigation';

import Timeline from "../TabNavigation/Tabs/TimelineNavigator";
import Calendar from '../TabNavigation/Tabs/CalendarNavigator';
import Search from '../TabNavigation/Tabs/SearchNavigator';
import Posts from '../TabNavigation/Tabs/PostsNavigator';
import Profile from '../TabNavigation/Tabs/ProfileNavigator';

import Help from '../BaseNavigators/HelpNavigator';

import CustomTabBar from '../../components/CustomTabBar';

const HelpTabNavigator = createBottomTabNavigator(
    {
        Help,
        Timeline,
        Calendar,
        Profile,
        Posts,
        Search,
    },
    {
        tabBarComponent: CustomTabBar,
    }
);

export default HelpTabNavigator;