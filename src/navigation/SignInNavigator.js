import React from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { Platform } from 'react-native';
import SignupScreen from '../screens/SignupScreen';
import LoginScreen from "../screens/LoginScreen";
import SignInScreen from '../screens/SignInScreen';

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

export default createAppContainer(
    createStackNavigator(
        {
            SignInScreen: {
                navigationOptions: {
                    header: null
                },
                screen: SignInScreen
            },
            LoginScreen: {
                navigationOptions: {
                    header: null
                },
                screen: LoginScreen
            },
            SignupScreen: {
                navigationOptions: {
                    header: null
                },
                screen: SignupScreen
            }
        },
        config
    )
);