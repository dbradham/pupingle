import { createBottomTabNavigator } from 'react-navigation';

import CustomTabBar from '../../components/CustomTabBar';

import Notifications from './Tabs/ProfileNavigator';
import Timeline from "./Tabs/TimelineNavigator";
import Calendar from './Tabs/CalendarNavigator';
import Posts from './Tabs/PostsNavigator';
import Search from './Tabs/SearchNavigator';

const CustomTabNavigator = createBottomTabNavigator(
    {
        Timeline,
        Calendar,
        Notifications,
        Posts,
        Search
    },
    {
        tabBarComponent: CustomTabBar
    }
);

CustomTabNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0,
    tabBarOnPress: (scene, jumpToIndex) => {
        console.log('onPress:', scene.route);
        //jumpToIndex(scene.index);
    }
});

export default CustomTabNavigator;