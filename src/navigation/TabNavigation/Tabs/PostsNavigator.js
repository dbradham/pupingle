import { createStackNavigator } from "react-navigation";
import { Platform } from "react-native";
import Container from "../../../screens/PostsScreen";
import Post from "../../../components/Post";
import AccountProfile from "../../../components/AccountProfile";
import DogProfile from "../../../screens/DogProfileScreen";
import Comments from "../../../screens/CommentScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const PostsNavigator = createStackNavigator(
  {
    Container: {
      navigationOptions: {
        header: null
      },
      screen: Container
    },

    Post: {
      navigationOptions: {
        header: null
      },
      screen: Post
    },
    AccountProfile: {
      navigationOptions: {
        header: null
      },
      screen: AccountProfile
    },
    DogProfile: {
      navigationOptions: {
        header: null
      },
      screen: DogProfile
    },
    Comments: {
      navigationOptions: {
        header: null
      },
      screen: Comments
    }
  },
  config
);

PostsNavigator.navigationOptions = ({ navigation }) => ({
  tabBarVisible: navigation.state.index === 0,
  swipeEnabled: navigation.state.index === 0
});

export default PostsNavigator;
