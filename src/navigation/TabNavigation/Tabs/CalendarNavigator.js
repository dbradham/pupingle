import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import Main from '../../../components/Calendar/Main';
import Secondary from "../../../components/Calendar/Secondary";

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const CalendarNavigator = createStackNavigator(
    {
        Main: {
            navigationOptions: {
                header: null
            },
            screen: Main
        },

        Secondary: {
            navigationOptions: {
                header: null
            },
            screen: Secondary
        }
    },
    config
);

CalendarNavigator.navigationOptions = ({ navigation }) => ({
    tabBarVisible: navigation.state.index === 0,
    swipeEnabled: navigation.state.index === 0
});

export default CalendarNavigator;