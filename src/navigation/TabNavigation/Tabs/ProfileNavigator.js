import { createStackNavigator } from "react-navigation";
import { Platform } from 'react-native';
import AccountProfile from "../../../components/AccountProfile";
import SpecificNotification from '../../../components/Notifications/SpecificNotification';
import DogProfile from '../../../screens/DogProfileScreen';

const config = Platform.select({
    web: { headerMode: 'screen' },
    default: {},
});

const ProfileNavigator = createStackNavigator(
    {
        AccountProfile: {
            navigationOptions: {
                header: null
            },
            screen: AccountProfile
        },

        SpecificNotification: {
            navigationOptions: {
                header: null
            },
            screen: SpecificNotification
        },
        DogProfile: {
            navigationOptions: {
                header: null
            },
            screen: DogProfile
        }
    },
    config
);

ProfileNavigator.navigationOptions = ({ navigation }) => ({
    swipeEnabled: navigation.state.index === 0
});
export default ProfileNavigator;