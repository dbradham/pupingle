import { createStackNavigator } from "react-navigation";
import { Platform } from "react-native";
import HomeScreen from "../../../screens/HomeScreen";
import AccountProfile from "../../../components/AccountProfile";
import Comments from "../../../screens/CommentScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const TimelineNavigator = createStackNavigator(
  {
    Timeline: {
      navigationOptions: {
        header: null
      },
      screen: HomeScreen
    },
    Profile: {
      navigationOptions: {
        header: null
      },
      screen: AccountProfile
    },
    Comments: {
      navigationOptions: {
        header: null
      },
      screen: Comments
    }
  },
  config
);

TimelineNavigator.navigationOptions = ({ navigation }) => ({
  swipeEnabled: navigation.state.index === 0
});
export default TimelineNavigator;
