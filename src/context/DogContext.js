import createDataContext from './createDataContext';
import firebase from '../../firebase';

const getDogsForUserFromFirestore = async (uid) => {
  const dogs = await firebase
  .firestore()
  .collection('dogs')
  .where('uid', '==', uid)
  .get()
  .then((query) => {
    return query.docs.map((doc) => {
        return doc.data();
    });
  });
  return dogs;
};

const createDogInFirestore = (dog) => {
  const dogs = firebase.firestore().collection('dogs');
  async function add(dog) {
    await dogs.add(dog);
  }
  add(dog);
};

const updateDogInFirestore = (dog) => {
    // do stuff
}

const dogReducer = (state, action) => {
  switch (action.type) {
    case 'changeOne':
        let dogs = state.dogs;
        dogs.push(action.payload);
        return { ...state, dogs: dogs };
    case 'getForUser':
        return { ...state, dogs: action.payload }
    default:
        return state;
  }
};

const create = (dispatch) => (dog) => {
  createDogInFirestore(dog);
  dispatch({ type: 'changeOne', payload: dog });
}

const update = dispatch => (dog) => {
    updateDogInFirestore(dog);
    dispatch({ type: 'changeOne', payload: dog });
};

const getForUser = dispatch => (uid) => {
  const dogs = getDogsForUserFromFirestore(uid);
  dispatch({ type: 'getForUser', payload: dogs });
};

export const { Provider, Context } = createDataContext(
  dogReducer,
  { create, update, getForUser },
  { dogs: null }
);