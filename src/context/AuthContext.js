import createDataContext from './createDataContext';
import firebase from '../../firebase';
import { navigate } from '../navigationRef';

const addBuddyInFirebase = async (uid, buddyId) => {
  firebase
  .firestore()
  .collection('users')
  .where('uid', '==', uid)
  .get()
  .then((query) => {
    let user = query.docs[0];
    let buddies = user.data().buddies;
    buddies.push(buddyId);
    user.ref.update({
      buddies: buddies
    });
  });
};

const getUserFromFirestore = async (uid) => {
  try {
    const user = await firebase
    .firestore()
    .collection('users')
    .where('uid', '==', uid)
    .get()
    .then((query) => {
      return query.docs[0].data();
    });
    return user;
  } catch (error) {
    console.log('error getting user from firestore:', error);
    return null;
  }
};

const createUserInFirestore = (user) => {
  const users = firebase.firestore().collection('users');
  async function add(user) {
    await users.add({ ...user, dogs: [], buddies: [] });
  }
  add(user);
};

const getDogsForUserFromFirestore = async (uid) => {
  const dogs = await firebase
  .firestore()
  .collection('dogs')
  .where('uid', '==', uid)
  .get()
  .then((query) => {
    return query.docs.map((doc) => {
        return doc.data();
    });
  });
  return dogs;
};

const createDogInFirestore = (dog) => {
  const dogs = firebase.firestore().collection('dogs');
  async function add(dog) {
    await dogs.add(dog);
  }
  add(dog);
};

const updateDogInFirestore = (dog) => {
    // do stuff
}

const authReducer = (state, action) => {
  switch (action.type) {
    case 'signin':
        return { ...state, user: action.payload };
    case 'signout':
        return { ...state, user: null };
    case 'createOrUpdateDog':
      let dogs;
      if (state.dogs != null) {
        dogs = state.dogs;
        dogs.push(action.payload);
      } else {
        dogs = [action.payload];
      }
        return { ...state, dogs: dogs };
    case 'getDogsForUser':
        return { ...state, dogs: action.payload };
    default:
        return state;
  }
};

const checkAuth = (dispatch) => () => {
  firebase.auth().onAuthStateChanged(async (user) => {
    if (user != null) {
      const uid = user.uid;
      const firestoreUser = await getUserFromFirestore(uid).then((user) => {
        return user;
      });
      if (firestoreUser == null) {
        dispatch({ type: 'signup', payload: true });
        navigate('authFlow');
        return;
      }
      dispatch({ type: 'signin', payload: firestoreUser });
      getDogsForUserFromFirestore(firestoreUser.uid).then((dogs) => {
        dispatch({ type: 'getDogsForUser', payload: dogs });
      });
      navigate('mainFlow');
    } else {
      dispatch({ type: 'signup', payload: true });
      navigate('authFlow');
    }
  });
}

const signup = dispatch => (email, password, firstName, lastName) => {
    firebase.auth().createUserWithEmailAndPassword(email, password).then(({ user }) => {
      const uid = user.uid;
      createUserInFirestore({ email, password, firstName, lastName, uid });
    });
};

const login = dispatch => (email, password) => {
  try {
    firebase.auth().signInWithEmailAndPassword(email, password);
  } catch (error) {
    console.log({
      success: false,
      error: error.message || error
    });
  }
};

const signout = dispatch => () => {
    firebase.auth().signOut();
};

const createDog = (dispatch) => (dog) => {
  createDogInFirestore(dog);
  dispatch({ type: 'createOrUpdateDog', payload: dog });
}

const updateDog = dispatch => (dog) => {
    updateDogInFirestore(dog);
    dispatch({ type: 'createOrUpdateDog', payload: dog });
};

const addBuddy = dispatch => (buddyId, user) => {
  const { email, firstName, lastName, uid, password } = user;
  addBuddyInFirebase(uid, buddyId);
  let buddies = user.buddies;
  buddies.push(buddyId);
  const updatedUser = {
    email,
    password,
    firstName,
    lastName,
    uid,
    buddies
  };
  dispatch({ type: 'signin', payload: updatedUser })
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { checkAuth, login, signup, signout, createDog, updateDog, addBuddy },
  { user: null, dogs: null }
);