import React from 'react';
import { View, StyleSheet } from 'react-native';
import Timeline from '../components/Timeline';
import CustomHeader from '../components/CustomHeader';

const HomeScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <CustomHeader title="Puppingle" navigation={navigation} />
            <Timeline navigation={navigation} />
        </View>
    );
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#3768a3',
    },
});