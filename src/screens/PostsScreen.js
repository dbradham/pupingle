import React, { useReducer } from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
} from 'react-native';
import CustomHeader from '../components/CustomHeader';
import List from '../components/List';
import ButtonGroup from '../components/ButtonGroup';

// state = { mode, offMode, bgIndex }
// action = { payload }
const reducer = (state, action) => {
    switch (action) {
        case 'recent':
            return { mode: 'recent', offMode: 'browse', bgIndex: 1 };
        default:
            return { mode: 'browse', offMode: 'recent', bgIndex: 0 };
    }
};

const PostsScreen = (props) => {
    const [state, dispatch] = useReducer(reducer, { mode: 'browse', offMode: 'recent', bgIndex: 0 });

    const { mode, offMode, bgIndex } = state;

    const buttonList = ['Browse', 'Recent'];

    return (
        <View style={styles.container}>
            <CustomHeader title="Posts" navigation={props.navigation} />

            <ButtonGroup buttons={buttonList} 
                index={bgIndex} 
                onPress={dispatch}
                offMode={offMode} 
                mode={mode} 
                style={styles.buttonGroup} />

            <List mode={state.mode} navigation={props.navigation} />
        </View>
    );
};

export default PostsScreen;

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    buttonGroup: { 
        height: 30,
        width: deviceWidth * 0.8,
    },
});