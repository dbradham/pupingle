import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CustomHeader from '../components/CustomHeader';

const SearchScreen = (props) => {
    return (
        <View>
            <CustomHeader title="Explore" navigation={props.navigation} />
            <Text>
                This is gonna be the search screen
            </Text>
        </View>
    );
};

export default SearchScreen;

const styles = StyleSheet.create({

});