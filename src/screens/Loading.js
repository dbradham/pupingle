import React, { useContext, useEffect } from 'react';
import { View, Image, StyleSheet, Text, Dimensions } from 'react-native';
import { Context } from '../context/AuthContext';

const Loading  = () =>  {
    const { checkAuth } = useContext(Context);
    useEffect(() => {
        checkAuth();
    }, []);
    
    const logo = require('../assets/images/loadingLogo.png');

    return (
        <View style={styles.container}>
            <Image source={logo} style={styles.logo} />
            <View style={styles.footer}>
                <Text style={styles.welcome}>Welcome to</Text>
                <Text style={styles.puppingle}>Puppingle</Text>
            </View>
        </View>
    );    
}

export default Loading;

const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    logo: {
        height: 350,
        width: 250,
        marginTop: deviceHeight * 0.15,
    },
    footer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
        position: 'absolute',
        bottom: 20,
    },
    welcome: {
        color: '#cc5500',
        fontSize: 16,
    },
    puppingle: {
        color: '#32A6DB',
        fontWeight: 'bold',
        fontSize: 24
    }
});