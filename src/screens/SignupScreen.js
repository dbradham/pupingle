import React, { useState, useContext } from 'react';
import { View, StyleSheet, Text, Dimensions, ImageBackground } from 'react-native';
import { Item, Form, Input, Button, Label } from "native-base";
import { Context } from '../context/AuthContext';

const SignupScreen = () => {
    const { state, signup } = useContext(Context);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return (
        <ImageBackground
         style={styles.container}
            source={{ uri: 'https://i.redd.it/dltqk6q2w0oz.jpg' }}>
                <Form>
                <View style={styles.form}>
                <Item floatingLabel
                        style={styles.item}>
                        <Label>First Name</Label>
                        <Input
                            value={firstName}
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={firstName => setFirstName(firstName)}
                        />
                    </Item>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label>Last Name</Label>
                        <Input
                            value={lastName}
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={lastName => setLastName(lastName)}
                        />
                    </Item>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label>Email</Label>
                        <Input
                            value={email}
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={email => setEmail(email)}
                        />
                    </Item>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label>Password</Label>
                        <Input
                            value={password}
                            style={styles.input}
                            secureTextEntry={true}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={password => setPassword(password)}
                        />
                    </Item>
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button full rounded
                            style={styles.button}
                            onPress={() => signup(email, password, firstName, lastName)}>
                            <Text style={styles.buttonText}>Sign Up</Text>
                        </Button>
                    </View>
            </Form>
        </ImageBackground>
    );
    
}

export default SignupScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-start"
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    item: {
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        width: 250,
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 75
    },
    button: {
        width: 150,
        height: 50,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: '#0033ee',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    }
});