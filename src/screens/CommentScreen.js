import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  FlatList,
  TextInput,
  Keyboard
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

import firebase from "../../firebase";

const fs = firebase.firestore();

const CommentScreen = ({ navigation }) => {
  const { type, contentId, userId } = navigation.state.params;
  const [comment, setComment] = useState("");
  const [comments, setComments] = useState([]);

  const leaveComment = () => {
    let model = {
      time: new Date(),
      id: contentId,
      userId: userId,
      comment
    };
    if (comment) {
      fs.collection(`${type}Comments`).add(model);
      setComment("");
    }
  };

  const getComments = async () => {
    const commentsCollection = fs.collection(`${type}Comments`);

    let result = await commentsCollection
      .where("id", "==", contentId)
      .get()
      .then(query => {
        return query.docs.map(doc => {
          return { ...doc.data(), commentId: doc.id };
        });
      });

    result = result.sort((a, b) => {
      return new Date(a.time) > new Date(b.time);
    });

    setComments(result);
  };

  useEffect(() => {
    getComments();
  }, []);

  return (
    <View style={{ flex: 1, marginTop: 30 }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <TouchableWithoutFeedback
          style={{ flex: 1 }}
          onPress={() => Keyboard.dismiss()}
        >
          <View style={{ flex: 1 }}>
            <FlatList
              data={comments}
              keyExtractor={item => item.commentId}
              renderItem={({ item }) => (
                <Text style={{ marginHorizontal: 10, marginVertical: 5 }}>
                  {item.comment}
                </Text>
              )}
            />
            <View
              style={{
                flexDirection: "row",
                marginHorizontal: 10,
                marginBottom: 20
              }}
            >
              <TextInput
                style={{
                  flex: 1,
                  borderColor: "#ccc",
                  borderWidth: 1,
                  borderRadius: 5
                }}
                value={comment}
                onChangeText={e => {
                  setComment(e);
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  leaveComment();
                }}
              >
                <MaterialIcons
                  name="send"
                  size={32}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </View>
  );
};

export default CommentScreen;

const styles = StyleSheet.create({});
