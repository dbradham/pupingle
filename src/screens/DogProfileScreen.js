import React, { useState, useEffect } from "react";
import { View,
Image,
Dimensions,
StyleSheet,
FlatList,
TouchableOpacity,
ActivityIndicator} from "react-native";
import CustomHeader from '../components/CustomHeader';
import DogProfileDetail from '../components/DogProfileDetail';
import PostInput from "../components/PostInput";
import firebase from '../../firebase';

const DogProfileScreen = ({ navigation }) => {
    const data = [
        {
            key: '0',
        },
        {
            key: '1',
        },
        {
            key: '2',
        },
        {
            key: '3',
        },
        {
            key: '4',
        },
        {
            key: '5',
        },
        {
            key: '6',
        },
        {
            key: '7',
        },
        {
            key: '8',
        },
        {
            key: '9',
        },
        {
            key: '10',
        },
        {
            key: '11',
        },
        {
            key: '12',
        },
        {
            key: '13',
        },
        {
            key: '14',
        },
    ];

    const [dogAvatar, setDogAvatar] = useState('');

    const getDogProfileImage = (dog) => {
        var storage = firebase.storage();
        var storageRef = storage.ref();
    
        let pathReference = storageRef.child("DogAvatars/" + dog.name);
    
        pathReference.getDownloadURL().then((url) => {
            setDogAvatar(url);
        })
    }

    useEffect(() => {
        getDogProfileImage(dog);
    }, []);

    const src3 = require('../assets/images/dog3.png');

    let dog;
    let user;

    navParams = navigation.state.params;
    if (navParams != null) {
        dog = navParams.dog;
        user = navParams.user;
    } else {
        return <ActivityIndicator />;
    }

    const breed = dog.breed;
    const name = dog.name;
    const gender = dog.gender;
    const birthday = new Date(dog.birthday.seconds * 1000);
    const birthmonth = birthday.getUTCMonth();
    const birthyear = birthday.getUTCFullYear();
    const now = new Date();
    const thisMonth = now.getUTCMonth();
    const thisYear = now.getUTCFullYear();
    const today = now.getDate();
    const age = thisMonth >= birthmonth && today >= birthday.getDate() ? thisYear - birthyear: thisYear - birthyear - 1;
    let ageString = '';
    if (age == 0) {
        ageString = 'Less than a year old';
    } else if (age == 1) {
        ageString = '1 year old';
    } else {
        ageString = age + ' years old';
    }

    const dogProfileImage = dogAvatar != '' ? <Image source={{ uri: dogAvatar }} style={styles.smallImage} />
        : <ActivityIndicator size="large" color="black" />;

    const dogHeader = dogAvatar != '' ? <Image style={styles.bigImage} source={{ uri: dogAvatar }} /> 
        : <ActivityIndicator size="large" color="black" />;
        
    const postInput = dog.uid == user.uid ? <PostInput dog={dog} user={user} /> : null;

    return (
        <View style={styles.wrapper}>
            <CustomHeader navigation={navigation} title={name} />

            {dogHeader}
            
            {dogProfileImage}

            <DogProfileDetail age={ageString} breed={breed} gender={gender} name={name} />

            {postInput}

            <FlatList
                contentContainerStyle={styles.listContainer}
                data={data}
                renderItem={({ item }) => (
                    <TouchableOpacity>
                        <Image source={src3} style={styles.image} />
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.key}
                numColumns={3} />
        </View>
    );
};

export default DogProfileScreen;

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    listContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    image: {
        marginVertical: 1,
        marginHorizontal: 1,
        height: 100,
        width: deviceWidth * 0.3,
        borderRadius: 5,
        borderColor: 'orange',
        borderWidth: 2,
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
    },
    bigImage: {
        alignSelf: 'center',
        width: deviceWidth * 1,
        height: deviceHeight * 0.2225,
        // borderRadius: deviceWidth * 0.15,
        borderBottomWidth: 2,
        borderColor: '#32A6DB',
    },
    smallImage: {
        flex: 1,
        width: deviceWidth * 0.2,
        height: deviceWidth * 0.2,
        borderRadius: deviceWidth * 0.1,
        borderWidth: 2,
        borderColor: 'red',
        position: 'absolute',
        left: deviceWidth * 0.7,
        top: (deviceHeight * 0.265),
    },
});