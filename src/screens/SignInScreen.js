import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import { Button } from "native-base";

const SignInScreen = (props) => {
    return (
        <ImageBackground 
            source={{ uri: 'https://cf.ltkcdn.net/dogs/images/std/236742-699x450-cutest-puppy-videos.jpg' }}
            style={styles.container} >
            <View style={styles.buttons}>
                <Button
                    rounded
                    style={styles.button}
                    onPress={() => props.navigation.navigate("Signup")}
                ><Text style={styles.buttonText}>Sign Up</Text></Button>
                <Button
                    rounded
                    style={styles.button}
                    onPress={() => props.navigation.navigate("Login")}
                ><Text style={styles.buttonText}>Login</Text></Button>
            </View>
            <View style={styles.headerContainer}>
                <Text style={styles.header}>Welcome to Pupingle!</Text>
            </View>
        </ImageBackground>
    );
};

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center'
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 75
    },
    header: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 36,
        textShadowRadius: 7.5,
        textShadowOffset: { width: 2.5, height: 2.5 },
        textShadowColor: '#0000ff'
    },
    buttons: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'column',
        height: 150
    },
    button: {
        width: 200,
        height: 50,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: '#0033ee',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    }
})