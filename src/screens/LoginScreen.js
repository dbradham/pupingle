import React, { useState, useContext } from 'react';
import { View, StyleSheet, Text, ImageBackground } from 'react-native';
import { Item, Form, Input, Button, Label } from "native-base";
import { Context } from '../context/AuthContext';

const LoginScreen = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { state, login } = useContext(Context);

    return (
        <ImageBackground
            style={styles.container}
            source={{ uri: 'http://www.zarias.com/wp-content/uploads/2015/12/61-cute-puppies.jpg' }}>
            <Button
                title="Back"
                onPress={() => this.props.navigation.navigate("SignIn")}
            />
            <View style={styles.headerContainer}>
                <Text style={styles.header}>Welcome Back!</Text>
            </View>
            <Form>
                <View style={styles.form}>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label>Email</Label>
                        <Input
                            value={email}
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={email => setEmail(email)}
                        />
                    </Item>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label>Password</Label>
                        <Input
                            value={password}
                            style={styles.input}
                            secureTextEntry={true}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={password => setPassword(password)}
                        />
                    </Item>
                </View>
                <View style={styles.buttonContainer}>
                    <Button full rounded
                        style={styles.button}
                        onPress={() => login(email, password)}>
                        <Text style={styles.buttonText}>Login</Text>
                    </Button>
                </View>
            </Form>
        </ImageBackground>
    );
};

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-start"
    },
    headerContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: 75
    },
    header: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 36,
        textShadowRadius: 7.5,
        textShadowOffset: { 
            width: 2.5, 
            height: 2.5 
        },
        textShadowColor: '#0000ff'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    item: {
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        width: 250,
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 75
    },
    button: {
        width: 150,
        height: 50,
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: '#0033ee',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold'
    }
});