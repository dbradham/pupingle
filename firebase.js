import { firebase } from '@firebase/app';
import '@firebase/auth';
import '@firebase/firestore'; 
import 'firebase/storage';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyAeEOWQV9JZJyysTNTLcpa5pLPQ9YiCprU",
    authDomain: "pupingle.firebaseapp.com",
    databaseURL: "https://pupingle.firebaseio.com",
    storageBucket: "pupingle.appspot.com",
    projectId: "pupingle"
};
firebase.initializeApp(firebaseConfig);

export default firebase;