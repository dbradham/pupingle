const clientPath = 'C:/Users/daveb_000/documents/wakanda-dog/client';
const bodyParser = require('body-parser');
const express = require("express");
//const { db } = require('./db');

const app = express();

const router = express.Router();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies
app.use('/', router);

app.set('views', './client');

app.set('view engine', 'ejs');

router.use(function (req, res, next) {
    console.log(req.method + ' ' + req.path + ' ' + Date().toString());
    next();
});

router.post('/signup', (req, res) => {
    try {
        console.log(req.body);
        res.json(req.body);
    } catch (error) {
        res.json({
            success: false,
            error: error.message || error
        });
    }    
})