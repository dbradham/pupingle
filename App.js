
import React from 'react';
import { 
    createDrawerNavigator, 
    createAppContainer,
    createSwitchNavigator,
    createStackNavigator
} from 'react-navigation';

import CustomDrawerNavigator from "./src/components/CustomDrawerNavigator";

import CustomTabNavigator from "./src/navigation/TabNavigation/HomeTabNavigator";

import SettingsTabNavigator from './src/navigation/DrawerNavigation/SettingsTabNavigator';
import MessagesTabNavigator from './src/navigation/DrawerNavigation/MessagesTabNavigator';
import GroupsTabNavigator from './src/navigation/DrawerNavigation/GroupsTabNavigator';
import NeighborhoodTabNavigator from './src/navigation/DrawerNavigation/NeighborhoodTabNavigator';
import NotificactionsTabNavigator from './src/navigation/DrawerNavigation/NotificationsTabNavigator';
import HelpTabNavigator from './src/navigation/DrawerNavigation/HelpTabNavigator';
import SignInScreen from './src/screens/SignInScreen';
import SignupScreen from './src/screens/SignupScreen';
import LoginScreen from './src/screens/LoginScreen';

import { Provider as AuthProvider } from './src/context/AuthContext';

import { setNavigator } from './src/navigationRef';
import Loading from './src/screens/Loading';
import SearchTabNavigator from './src/navigation/DrawerNavigation/SearchTabNavigator';

const TabStack = CustomTabNavigator;

MessagesTabNavigator.navigationOptions = {
    drawerLabel: "Messages"
};

NotificactionsTabNavigator.navigationOptions = {
    drawerLabel: 'Notifications'
};

GroupsTabNavigator.navigationOptions = {
    drawerLabel: "Groups"
};

NeighborhoodTabNavigator.navigationOptions = {
    drawerLabel: "Neighborhood"
};

HelpTabNavigator.navigationOptions = {
    drawerLabel: "Help & Support"
};

SettingsTabNavigator.navigationOptions = {
    drawerLabel: "Settings & Privacy"
};

const switchNavigator = createSwitchNavigator({
    loading: Loading,
    authFlow: createStackNavigator({
        Signin: SignInScreen,
        Signup: SignupScreen,
        Login: LoginScreen
    }),
    mainFlow: createDrawerNavigator(
        {
            TabStack,
            MessagesTabNavigator,
            NotificactionsTabNavigator,
            GroupsTabNavigator,
            NeighborhoodTabNavigator,
            HelpTabNavigator,
            SettingsTabNavigator,
            SearchTabNavigator
        },
        {
          contentComponent: CustomDrawerNavigator
        }
    )
});

const App = createAppContainer(switchNavigator);

export default () => {
    return (
        <AuthProvider>
            <App ref={(navigator) => { setNavigator(navigator) }}/>
        </AuthProvider>
    );
}